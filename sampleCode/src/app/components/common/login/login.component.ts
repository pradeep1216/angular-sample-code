import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../../../services/common/login/login.service';
import { AsyncResponse } from '../../../interfaces/async-response';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ContainerService } from 'src/app/services/admin/container/container.service';
import { AuthService } from 'src/app/services/common/auth/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styles: [
    ]
})
export class LoginComponent implements OnInit {

    activeBg = true;
    data;
    title;
    currentRoute;
    submitted = false;

    loginForm = this.fb.group({
        username: ['', [
            Validators.required,
        ]],
        password: ['', [
            Validators.required,
            Validators.maxLength(25),
        ]]
    });
    user = this.auth.currentUserValue;

    constructor(
        private loginService: LoginService,
        private router: Router,
        private fb: FormBuilder,
        private snackBar: MatSnackBar,
        private containerService: ContainerService,
        private auth: AuthService
    ) { }

    ngOnInit(): void {
        this.getCurrentRoute();
    }

    get f() { return this.loginForm.controls; }

    login(currentRoute) {
        this.submitted = true;
        if (this.loginForm.valid) {
            this.submitted = true;

            this.data = {
                username: this.loginForm.get('username').value,
                password: this.loginForm.get('password').value,
            };

            this.loginService.login(this.data, currentRoute).subscribe(
                (response: AsyncResponse) => {
                    this.snackBar.open('Login Successful.', null, { duration: 3500, panelClass: ['snackbar-success'] });
                    if (currentRoute === 'admin') {
                        this.containerService.setActiveRoute('homeRoute');
                        this.router.navigate(['/admin/home']);
                    } else if (currentRoute === 'user') {
                        this.containerService.setActiveRoute('userHomeRoute');
                        this.router.navigate(['/user/home']);
                    }

                }, (error: AsyncResponse) => {
                    this.snackBar.open(error['message'], null, { duration: 3500, panelClass: ['snackbar-danger'] });
                    this.loginForm.reset();
                    this.submitted = false;
                });
        }

    }

    getCurrentRoute() {
        if (this.router.url === '/admin/login' || this.router.url.includes('/admin/login?fbclid')) {
            this.title = 'Admin Login';
            this.currentRoute = 'admin';
        } else if (this.router.url === '/user/login' || this.router.url.includes('/user/login?fbclid')) {
            this.title = 'User Login';
            this.currentRoute = 'user';
        }
    }

    changeMenu(nextRoute) {
        if (nextRoute === 'user') {
            this.router.navigate(['/user/login'])
        } else if (nextRoute === 'admin') {
            this.router.navigate(['/admin/login'])
        }

    }

}
