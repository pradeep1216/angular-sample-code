import { Component, ElementRef, Input, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import videojs from 'video.js';


@Component({
    selector: 'app-vjs-player',
    template: `
    <video #target class="video-js" controls muted playsinline preload="none"  height="400px"></video>
  `,
    styleUrls: ['./vjs-player.component.scss']
})
export class VjsPlayerComponent implements OnInit {

    @ViewChild('target', { static: true }) target: ElementRef;
    // see options: https://github.com/videojs/video.js/blob/mastertutorial-options.html
    @Input() options: {
        fluid: boolean,
        aspectRatio: string,
        autoplay: boolean,
        sources: {
            src: string,
            type: string,
        }[],
    };
    player: videojs.Player;

    constructor(
        private elementRef: ElementRef,
    ) { }

    ngOnInit() {
        // instantiate Video.js
        this.player = videojs(this.target.nativeElement, this.options, function onPlayerReady() {
            console.log('onPlayerReady', this);
        });
    }

    ngOnDestroy() {
        // destroy player
        if (this.player) {
            this.player.dispose();
        }
    }

    playPause() {
        var myVideo: any = document.getElementById("target");
        if (myVideo.paused) myVideo.play();
        else myVideo.pause();
    }

}
