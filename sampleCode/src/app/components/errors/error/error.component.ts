import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/common/auth/auth.service';


@Component({
    selector: 'app-error',
    templateUrl: './error.component.html',
    styles: [
    ]
})
export class ErrorComponent implements OnInit {

    errorCode;
    errorMessage;
    currentUser;
    redirectRoute;
    userRole;

    constructor(
        private router: Router,
        private auth: AuthService
    ) { }

    ngOnInit(): void {
        this.getCurrentError();
    }

    getCurrentError() {
        if (this.router.url === '/admin/access-forbidden') {
            this.errorCode = 403;
            this.errorMessage = 'ACCESS FORBIDDEN';
        } else {
            this.errorCode = 404;
            this.errorMessage = 'PAGE NOT FOUND';
        }
        this.getCurrentUser();

    }

    getCurrentUser() {
        if (this.auth.getUserDetails() === 'A' || this.auth.getUserDetails() === 'SA' || this.auth.getUserDetails() === 'SF') {
            if (this.auth.getToken()) {
                this.redirectRoute = '/admin/home';
            } else {
                this.redirectRoute = '/admin/login';
            }

        } else if (this.auth.getUserDetails() === 'P' || this.auth.getUserDetails() === 'ST') {
            if (this.auth.getToken()) {
                this.redirectRoute = '/user/home'
            } else {
                this.redirectRoute = '/user/login';
            }
        }
        setTimeout(() => {
            this.router.navigate([this.redirectRoute])
        }, 3000);
    }

}
