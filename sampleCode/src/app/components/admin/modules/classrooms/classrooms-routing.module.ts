import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClassroomComponent } from './classroom/classroom.component';
import { ClassroomManagerComponent } from './classroom-manager/classroom-manager.component';

const routes: Routes = [
    {
        path: '',
        component: ClassroomComponent
    }, {
        path: 'add',
        component: ClassroomManagerComponent
    }, {
        path: ':class_id/edit',
        component: ClassroomManagerComponent
    }, {
        path: ':class_id/view',
        component: ClassroomManagerComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ClassroomsRoutingModule { }
