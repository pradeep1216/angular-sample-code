import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClassroomsRoutingModule } from './classrooms-routing.module';
import { ClassroomComponent } from './classroom/classroom.component';
import { ClassroomManagerComponent } from './classroom-manager/classroom-manager.component';
import { DataTablesModule } from 'angular-datatables';
import { ReactiveFormsModule } from '@angular/forms';
import { StaffTypePipe } from 'src/app/pipes/staff-type.pipe';
import { MatTabsModule } from '@angular/material/tabs';


@NgModule({
    declarations: [ClassroomComponent, ClassroomManagerComponent, StaffTypePipe],
    imports: [
        CommonModule,
        ClassroomsRoutingModule,
        DataTablesModule,
        ReactiveFormsModule,
        MatTabsModule,
    ]
})
export class ClassroomsModule { }
