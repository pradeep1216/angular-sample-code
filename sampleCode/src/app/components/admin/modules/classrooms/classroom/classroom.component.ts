import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { ClassroomService } from '../../../../../services/admin/classroom/classroom.service';
import { Subject } from 'rxjs';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from '../../../../../services/common/auth/auth.service';
import { ContainerService } from 'src/app/services/admin/container/container.service';

@Component({
    selector: 'app-classroom',
    templateUrl: './classroom.component.html',
    styleUrls: ['./classroom.component.scss']
})
export class ClassroomComponent implements OnInit {
    classes;
    data;
    modalRef: BsModalRef;
    userRole;
    submitted = false;

    @ViewChild(DataTableDirective, { static: false })
    dtElement: DataTableDirective;
    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<any> = new Subject();

    constructor(
        private classService: ClassroomService,
        private modalService: BsModalService,
        private snackBar: MatSnackBar,
        private auth: AuthService,
        private containerService: ContainerService

    ) { }

    dataLoaded = false;
    ngOnInit(): void {
        this.containerService.setActiveRoute('classroomRoute')
        this.getUserType()
        this.getAllClasses();
        this.dtOptions = {
            pageLength: 25
        };
    }

    getAllClasses() {
        this.classService.getClasses().subscribe(
            res => {
                this.dataLoaded = true
                this.classes = res['data']
                this.rerender()
            },
            err => {
            }
        );
    }

    delClassID;
    delClassName;
    delClassDesc;

    deleteClass(classId) {
        if (classId) {
            this.submitted = true;
            this.classService.deleteClass(classId).subscribe(
                res => {
                    this.snackBar.open('Class deleted successfully.', null, { duration: 3500, panelClass: ['snackbar-success'] });
                    this.closeModal();
                    this.getAllClasses();
                    this.submitted = false;
                }, err => {
                    this.snackBar.open('Failure.', null, { duration: 3500, panelClass: ['snackbar-danger'] });
                    this.submitted = false;
                    this.closeModal();
                }
            )
        } else {
            this.snackBar.open('Failure. Invalid option.', null, { duration: 3500, panelClass: ['snackbar-danger'] });
            this.closeModal();
        }

    }

    ngAfterViewInit(): void {
        this.dtTrigger.next();
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
    }

    rerender(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table `first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            this.dtTrigger.next();
        });
    }


    openModal(template: TemplateRef<any>, classId, className, classDescription) {
        this.modalRef = this.modalService.show(template);
        this.delClassID = classId;
        this.delClassName = className;
        this.delClassDesc = classDescription
    }

    closeModal() {
        const modalHeader = document.getElementsByClassName('modal-header');
        if (modalHeader && modalHeader.length > 0) {
            // Get the close button in the modal header
            const closeButton: any = modalHeader[0].getElementsByTagName('button');
            if (closeButton && closeButton.length > 0) {
                // simulate click on close button
                closeButton[0].click();
            }
        }
    }

    getUserType() {
        this.userRole = this.auth.getUserDetails();
    }
}
