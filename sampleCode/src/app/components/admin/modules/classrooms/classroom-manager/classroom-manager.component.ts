import { Component, OnInit, TemplateRef, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ClassroomService } from 'src/app/services/admin/classroom/classroom.service';
import { ContainerService } from 'src/app/services/admin/container/container.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { SubjectService } from 'src/app/services/admin/subjects/subject.service';

@Component({
    selector: 'app-classroom-manager',
    templateUrl: './classroom-manager.component.html',
    styleUrls: ['./classroom-manager.component.scss']
})
export class ClassroomManagerComponent implements OnInit, OnDestroy {

    title;
    dataLoaded = false;
    currentRoute;
    submitted = false;
    data;
    classId;

    constructor(
        private router: Router,
        private fb: FormBuilder,
        private classService: ClassroomService,
        private route: ActivatedRoute,
        private snackBar: MatSnackBar,
        private containerService: ContainerService,
        private modalService: BsModalService,
        private subjectService: SubjectService
    ) { }

    ngOnInit() {
        this.showActiveMenu();
        this.getCurrentRoute();
    }

    ngOnDestroy() {
        this.closeModal();
    }

    showActiveMenu() {
        if (this.containerService.currentRouteValue !== 'classroomRoute') {
            this.containerService.setActiveRoute('classroomRoute')
        }
    }

    getCurrentRoute() {
        if (this.router.url.includes('/classrooms/add')) {
            this.title = 'ADD A CLASSROOM';
            this.currentRoute = 'addClass';
            this.initializeAddClassForm()
        } else if (this.router.url.includes('/classrooms/') && this.router.url.includes('/edit')) {
            this.currentRoute = 'editClass';
            this.getClassId();
            this.getClass(this.classId, this.currentRoute);
            this.getAllSubjects(this.classId);
            this.getStaffs(this.classId);

        } else if (this.router.url.includes('/classrooms/') && this.router.url.includes('/view')) {

            this.currentRoute = 'viewClass';
            this.getClassId();
            this.getClass(this.classId, this.currentRoute);
            this.getAllSubjects(this.classId);
            this.getStaffs(this.classId);

        }

    }

    getClassId() {
        this.route.paramMap.subscribe((params: ParamMap) => {
            this.classId = this.route.snapshot.paramMap.get('class_id');
        });
    }

    // Add Class
    createClassForm;
    initializeAddClassForm() {
        this.createClassForm = this.fb.group({
            className: ['', [
                Validators.required,
            ]],
            division: ['', [
                Validators.required,
            ]],
            classDescription: [],
            studentType: ['', [
                Validators.required,
            ]],
        });

        this.createClassForm.patchValue({
            studentType: 'T',
        });
    }

    addClass() {
        if (this.createClassForm.valid) {
            this.submitted = true;

            if (this.createClassForm.get('classDescription').value) {
                this.data = {
                    class_title: this.createClassForm.get('className').value,
                    class_division: this.createClassForm.get('division').value,
                    class_description: this.createClassForm.get('classDescription').value,
                    student_type: this.createClassForm.get('studentType').value,
                }
            } else {
                this.data = {
                    class_title: this.createClassForm.get('className').value,
                    class_division: this.createClassForm.get('division').value,
                    student_type: this.createClassForm.get('studentType').value,
                }
            }
            this.classService.createClass(this.data).subscribe(
                res => {
                    this.snackBar.open('Class created successfully.', null, { duration: 3500, panelClass: ['snackbar-success'] });
                    this.router.navigateByUrl('admin/classrooms');
                }, err => {
                    this.snackBar.open(err.message, null, { duration: 3500, panelClass: ['snackbar-danger'] });
                    this.submitted = false;
                }
            )
        }
    }

    // View Class
    classRoom;
    editClassRoom;
    getClass(classId, currentRoute) {
        this.classService.getClass(classId).subscribe(
            res => {
                this.title = res['data']['class_name'];
                this.dataLoaded = true;
                this.classRoom = res['data'];
                if (currentRoute === 'editClass') {
                    this.initializeEditClassForm(this.classRoom);
                }

            },
            err => { }
        );
    }

    // Edit Class
    editClassForm;
    initializeEditClassForm(classDetails) {
        this.editClassForm = this.fb.group({
            className: ['', [
                Validators.required,
            ]],
            division: ['', [
                Validators.required,
            ]],
            classDescription: [],
            classStatus: ['', [
                Validators.required,
            ]],
            studentType: ['', [
                Validators.required,
            ]],
        });

        if (classDetails['class_title'] && classDetails['class_division'] !== null) {
            this.editClassForm.patchValue({
                className: classDetails['class_title'],
                division: classDetails['class_division'],
                classDescription: classDetails['class_description'],
                classStatus: classDetails['class_active'],
                studentType: classDetails['student_type']
            });
        } else {
            this.editClassForm.patchValue({
                className: classDetails['class_title'],
                division: classDetails['class_division'],
                classStatus: classDetails['class_active'],
                studentType: classDetails['student_type']
            });
        }
    }

    editClass() {
        if (this.editClassForm.valid) {
            this.submitted = true;
            let classId = this.classId

            if (this.editClassForm.get('classDescription').value) {
                this.data = {

                    class_title: this.editClassForm.get('className').value,
                    class_division: this.editClassForm.get('division').value,
                    class_description: this.editClassForm.get('classDescription').value,
                    class_active: this.editClassForm.get('classStatus').value,
                    student_type: this.editClassForm.get('studentType').value,
                }
            } else {
                this.data = {
                    class_title: this.editClassForm.get('className').value,
                    class_division: this.editClassForm.get('division').value,
                    class_description: '',
                    class_active: this.editClassForm.get('classStatus').value,
                    student_type: this.editClassForm.get('studentType').value,

                }
            }
            this.classService.updateClass(classId, this.data).subscribe(
                res => {
                    this.snackBar.open('Class edited successfully.', null, { duration: 3500, panelClass: ['snackbar-success'] });
                    this.router.navigateByUrl('admin/classrooms');
                }, err => {
                    this.snackBar.open(err.message, null, { duration: 3500, panelClass: ['snackbar-danger'] });
                    this.submitted = false;
                }
            )
        }
    }

    // Subjects
    subjects = [];
    subjectLoaded = false;
    getAllSubjects(classId) {
        this.subjectService.getClassSubjects(classId).subscribe(
            res => {
                this.subjectLoaded = true
                this.subjects = res['data']
            },
            err => {
            }
        );
    }

    // Staffs
    staffs = [];
    staffsLoaded = false;
    getStaffs(classId) {
        this.classService.getStaffList(classId).subscribe(
            res => {
                this.staffsLoaded = true;
                this.staffs = res['data']
            },
            err => {
                this.staffs = [];
                this.staffsLoaded = true;
            }
        );
    }

    currSubjectId;

    // Modal
    modalRef: BsModalRef;
    openSubjectModal(template: TemplateRef<any>, currentView, subjectId) {
        if (currentView === 'addSubject' || currentView === 'editSubject') {
            this.modalRef = this.modalService.show(template);
        }


        if (currentView === 'addSubject') {
            this.initializeAddSubjectForm(this.classId);
        } else if (currentView === 'deleteSubject') {
            if (this.staffs) {
                let subjectPresent = false;
                for (let i = 0; i < this.staffs['length']; i++) {
                    this.staffs[i]
                    for (let j = 0; j < this.staffs[i]['assign_staff']['length']; j++) {
                        if (this.staffs[i]['assign_staff'][j]['subject'] === subjectId) {
                            subjectPresent = true;
                        }
                    }
                }

                if (subjectPresent === false) {
                    this.currSubjectId = subjectId;
                    this.modalRef = this.modalService.show(template);

                } else {
                    this.snackBar.open('Failure. Staff assigned to subject cannot be deleted.', null, { duration: 3500, panelClass: ['snackbar-danger'] });
                }

            } else {
                this.currSubjectId = subjectId;
                this.modalRef = this.modalService.show(template);
            }

        } else if (currentView === 'editSubject') {
            this.currSubjectId = subjectId;
            this.getSubject(this.currSubjectId);
        }
    }

    addSubjectForm;
    // Add subject
    initializeAddSubjectForm(classId) {
        this.addSubjectForm = this.fb.group({
            classId: ['', [
                Validators.required,
            ]],
            subjectName: ['', [
                Validators.required,
            ]],
            subjectDescription: ['', [
                Validators.required,
            ]]
        });
        this.addSubjectForm.patchValue({
            classId: classId
        });
    }

    submittedModal = false;
    addSubject() {
        if (this.addSubjectForm.valid) {
            this.submittedModal = true;
            this.data = {
                class_id: this.addSubjectForm.get('classId').value,
                subject_name: this.addSubjectForm.get('subjectName').value,
                subject_description: this.addSubjectForm.get('subjectDescription').value,
            }
            this.subjectService.addSubject(this.data).subscribe(
                res => {
                    this.snackBar.open('Subject created successfully.', null, { duration: 3500, panelClass: ['snackbar-success'] });
                    this.getAllSubjects(this.classId);
                    this.closeModal();
                    setTimeout(() => {
                        this.submittedModal = false;
                    }, 500);

                }, err => {
                    this.submittedModal = false;
                    this.snackBar.open(err.message, null, { duration: 3500, panelClass: ['snackbar-danger'] });
                }
            )
        }
    }

    deleteSubject() {
        if (this.currSubjectId) {
            this.submittedModal = true;
            this.subjectService.deleteSubject(this.currSubjectId).subscribe(
                res => {
                    this.snackBar.open('Subject deleted successfully.', null, { duration: 3500, panelClass: ['snackbar-success'] });
                    this.closeModal();
                    this.getAllSubjects(this.classId);
                    setTimeout(() => {
                        this.submittedModal = false;
                    }, 500);

                }, err => {
                    this.snackBar.open(err.message, null, { duration: 3500, panelClass: ['snackbar-danger'] });
                    this.submittedModal = false;
                    this.closeModal();
                }
            )
        } else {
            this.snackBar.open('Failure. Invalid option.', null, { duration: 3500, panelClass: ['snackbar-danger'] });
            this.closeModal();
        }

    }

    subjectDetail;
    subjectDetailLoaded = false;
    getSubject(subjectId) {
        this.subjectService.getSubject(subjectId).subscribe(
            res => {
                this.subjectDetail = res['data'][0];
                this.initializeEditSubjectForm(this.subjectDetail);
                this.subjectDetailLoaded = true;
            },
            err => {
            }
        );
    }

    editSubjectForm;
    // Add subject
    initializeEditSubjectForm(data) {
        this.editSubjectForm = this.fb.group({
            classId: ['', [
                Validators.required,
            ]],
            subjectName: ['', [
                Validators.required,
            ]],
            subjectDescription: ['', [
                Validators.required,
            ]], status: ['', [
                Validators.required,
            ]],
        });
        this.editSubjectForm.patchValue({
            classId: data['classes'],
            subjectName: data['subject_name'],
            subjectDescription: data['subject_description'],
            status: data['subject_active']
        });
    }

    editSubject() {
        if (this.editSubjectForm.valid) {
            this.submittedModal = true;
            let subjectId = this.currSubjectId
            this.data = {
                subject_name: this.editSubjectForm.get('subjectName').value,
                subject_description: this.editSubjectForm.get('subjectDescription').value,
                subject_active: this.editSubjectForm.get('status').value,
            }
            this.subjectService.updateSubject(subjectId, this.data).subscribe(
                res => {
                    this.snackBar.open('Subject edited successfully.', null, { duration: 3500, panelClass: ['snackbar-success'] });
                    this.getAllSubjects(this.classId);
                    this.closeModal();
                    setTimeout(() => {
                        this.submittedModal = false;
                    }, 500);

                }, err => {
                    this.submittedModal = false;
                    this.snackBar.open(err.message, null, { duration: 3500, panelClass: ['snackbar-danger'] });
                }
            )
        }
    }


    assignId;
    openStaffModal(template: TemplateRef<any>, currentView, assignId) {
        this.modalRef = this.modalService.show(template);
        this.classSubjectsLoaded = false
        if (currentView === 'addStaff') {
            this.initializeStaffAddForm();
            this.getAllStaffs();
            this.clearDetails();
        } else if (currentView === 'editStaff') {
            this.assignId = assignId;
            this.getStaff(this.assignId);
            this.clearDetails();
        } else if (currentView === 'deleteStaff') {
            this.assignId = assignId;
        }
    }

    allStaffs = [];
    getAllStaffs() {
        this.classService.getAllStaffs(this.classId).subscribe(
            res => {
                this.allStaffs = res['data'];
            }
        )
    }

    clearDetails() {
        this.classSubjects = [];
        this.mainStaff = []
    }

    staffSelected(staffId) {
        this.getClassSubjects(staffId);
        this.checkMainStaffExists();
    }

    classSubjects;
    classSubjectsLoaded = false;
    getClassSubjects(staffId) {
        this.classService.getAllSubjects(this.classId, staffId).subscribe(
            res => {
                this.classSubjects = res['data'];
                this.classSubjectsLoaded = true;
            },
            err => {
            }
        );
    }

    mainStaff;
    checkMainStaffExists() {
        this.classService.getMainExists(this.classId).subscribe(
            res => {
                this.mainStaff = res['data']
            },
            err => {
            }
        );
    }


    staffAddForm;
    initializeStaffAddForm() {
        this.staffAddForm = this.fb.group({
            staffId: ['', [
                Validators.required,
            ]],
            classId: ['', [
                Validators.required,
            ]],
            subjectId: this.fb.array([]),
            staffType: ['', [
                Validators.required,
            ]]
        });
        this.staffAddForm.patchValue({
            classId: this.classId
        });
    }


    // subjectSelected = []
    onCheckboxChange(e) {

        const checkArray: FormArray = this.staffAddForm.get('subjectId') as FormArray;
        if (e.target.checked) {
            checkArray.push(new FormControl(e.target.value));
        } else {
            let i: number = 0;
            checkArray.controls.forEach((item: FormControl) => {
                if (item.value == e.target.value) {
                    checkArray.removeAt(i);
                    return;
                }
                i++;
            });
        }
    }

    // Add staff
    addStaff() {
        if (this.staffAddForm.valid) {
            this.submittedModal = true;
            this.data = {
                staff_id: this.staffAddForm.get('staffId').value,
                class_id: this.staffAddForm.get('classId').value,
                subject_ids: this.staffAddForm.get('subjectId').value,
                type_of_staff: this.staffAddForm.get('staffType').value,
            }

            this.classService.addStaff(this.data).subscribe(
                res => {
                    this.snackBar.open('Staff added successfully.', null, { duration: 3500, panelClass: ['snackbar-success'] });
                    this.closeModal();
                    this.getStaffs(this.classId);
                    this.submittedModal = false;
                    this.clearDetails();
                }, err => {
                    this.submittedModal = false;
                    this.snackBar.open('Failure.', null, { duration: 3500, panelClass: ['snackbar-danger'] });
                }
            )
        }
    }

    deleteStaff() {
        if (this.assignId) {
            this.submittedModal = true;
            this.classService.deleteStaff(this.assignId).subscribe(
                res => {
                    this.snackBar.open('Staff deleted successfully.', null, { duration: 3500, panelClass: ['snackbar-success'] });
                    this.getStaffs(this.classId);
                    this.closeModal();
                    setTimeout(() => {
                        this.submittedModal = false;
                    }, 1000);
                }, err => {
                    this.snackBar.open(err.message, null, { duration: 3500, panelClass: ['snackbar-danger'] });
                    this.submittedModal = false;
                    this.closeModal();
                }
            )
        }
    }


    staffDetail;
    staffDetailLoaded;
    getStaff(staffAssignId) {
        this.classService.getStaff(staffAssignId).subscribe(
            res => {
                this.staffDetail = res['data'];
                this.staffDetailLoaded = true;
                this.initializeStaffEditForm(this.staffDetail)
            },
            err => {
                this.staffDetailLoaded = true;
            }
        );
    }

    deleteSingleSubject(subjectId, staffAssignId) {
        if (subjectId) {
            this.classService.deleteStaffSubject(staffAssignId, subjectId).subscribe(
                res => {
                    this.snackBar.open('Staff subject deleted successfully.', null, { duration: 3500, panelClass: ['snackbar-success'] });
                    this.getStaffs(this.classId);
                    this.getStaff(staffAssignId);
                    setTimeout(() => {
                        this.submittedModal = false;
                    }, 1000);
                }, err => {
                    this.snackBar.open('Failure.', null, { duration: 3500, panelClass: ['snackbar-danger'] });
                    this.submittedModal = false;
                    this.closeModal();
                }
            )
        }
    }



    closeModal() {
        const modalHeader = document.getElementsByClassName('modal-header');
        if (modalHeader && modalHeader.length > 0) {
            // Get the close button in the modal header
            const closeButton: any = modalHeader[0].getElementsByTagName('button');
            if (closeButton && closeButton.length > 0) {
                // simulate click on close button
                closeButton[0].click();
            }
        }
    }

    staffEditForm;
    initializeStaffEditForm(staff) {
        this.staffEditForm = this.fb.group({
            staffId: ['', [
                Validators.required,
            ]],
            classId: ['', [
                Validators.required,
            ]],
            subjectId: this.fb.array([], Validators.required),
            staffType: ['', [
                Validators.required,
            ]]
        });
        this.getClassSubjects(staff['staff'])
        this.staffEditForm.patchValue({
            classId: this.classId,
            staffId: staff['staff'],
            staffType: staff['type_of_staff']
        });
    }

    editStaff() {
        this.data = {
            staff_id: this.staffEditForm.get('staffId').value,
            class_id: this.staffEditForm.get('classId').value,
            subject_ids: this.staffEditForm.get('subjectId').value,
            type_of_staff: this.staffEditForm.get('staffType').value,
        }

        this.classService.updateStaff(this.assignId, this.data).subscribe(
            res => {
                this.snackBar.open('Subject edited successfully.', null, { duration: 3500, panelClass: ['snackbar-success'] });
                this.closeModal();
                this.getStaffs(this.classId);
                this.submittedModal = false;
                this.clearDetails();

            }, err => {
                this.snackBar.open(err.message, null, { duration: 3500, panelClass: ['snackbar-danger'] });
                this.submitted = false;
            }
        )
    }

    onCheckboxChangeEdit(e) {
        const checkArray: FormArray = this.staffEditForm.get('subjectId') as FormArray;
        if (e.target.checked) {
            checkArray.push(new FormControl(e.target.value));
        } else {
            let i: number = 0;
            checkArray.controls.forEach((item: FormControl) => {
                if (item.value == e.target.value) {
                    checkArray.removeAt(i);
                    return;
                }
                i++;
            });
        }

        // console.log(emailFormArray)
    }
}
