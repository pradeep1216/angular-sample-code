import { Component, OnInit } from '@angular/core';
import { ContainerService } from 'src/app/services/admin/container/container.service';
import { AuthService } from 'src/app/services/common/auth/auth.service';
import { UserService } from 'src/app/services/admin/users/user.service';
declare var $: any;
@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    userRole;
    userCount;
    dataLoaded = false;



    constructor(
        private containerService: ContainerService,
        private auth: AuthService,
        private userService: UserService
    ) { }

    ngOnInit(): void {
        this.containerService.setActiveRoute('homeRoute');
        this.getUserType();
        this.getUserCount();
    }

    getUserType() {
        this.userRole = this.auth.getUserDetails();
    }

    getUserCount() {
        this.userService.getUserCount().subscribe(
            res => {
                this.dataLoaded = true;
                this.userCount = res['data']
            },
            err => {
            }
        );
    }
}
