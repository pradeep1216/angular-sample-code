import { Component, OnInit } from '@angular/core';
import { ClassroomService } from 'src/app/services/admin/classroom/classroom.service';
import { ContainerService } from 'src/app/services/admin/container/container.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-st-classes',
    templateUrl: './st-classes.component.html',
    styleUrls: ['./st-classes.component.scss']
})
export class StClassesComponent implements OnInit {

    constructor(
        private classService: ClassroomService,
        private containerService: ContainerService,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.showActiveMenu();
        this.getAllClasses();
        this.removeClass();
    }

    showActiveMenu() {
        if (this.containerService.currentRouteValue !== 'staffDiscussionsRoute') {
            this.containerService.setActiveRoute('staffDiscussionsRoute')
        }
    }

    dataLoaded = false;
    classes = [];
    getAllClasses() {
        this.classService.getClasses().subscribe(
            res => {
                this.dataLoaded = true
                this.classes = res['data']
            },
            err => {
                this.dataLoaded = true
            }
        );
    }

    goToClass(classDetail) {
        let redirectURL = '/admin/staff-qa/' + classDetail.class_id + '/discussions';
        this.router.navigateByUrl(redirectURL);
        this.containerService.setClassDetailAdmin(classDetail)
    }

    removeClass() {
        this.containerService.removeClassDetailAdmin();
    }
}
