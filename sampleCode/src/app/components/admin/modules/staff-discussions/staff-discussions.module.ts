import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StaffDiscussionsRoutingModule } from './staff-discussions-routing.module';
import { StDiscussionsComponent } from './st-discussions/st-discussions.component';
import { StClassesComponent } from './st-classes/st-classes.component';
import { StDiscussionManagerComponent } from './st-discussion-manager/st-discussion-manager.component';
import { MatTabsModule } from '@angular/material/tabs';
import { DataTablesModule } from 'angular-datatables';
import { ReactiveFormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';
import { PipesModule } from 'src/app/modules/pipes.module';


@NgModule({
    declarations: [StDiscussionsComponent, StClassesComponent, StDiscussionManagerComponent],
    imports: [
        CommonModule,
        StaffDiscussionsRoutingModule,
        MatTabsModule,
        DataTablesModule,
        ReactiveFormsModule,
        QuillModule.forRoot(
            {
                modules: {
                    toolbar: [
                        ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
                        ['blockquote', 'code-block'],

                        [{ 'header': 1 }, { 'header': 2 }],               // custom button values
                        [{ 'list': 'ordered' }, { 'list': 'bullet' }],
                        [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
                        [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
                        [{ 'direction': 'rtl' }],                         // text direction

                        [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
                        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

                        [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
                        [{ 'font': [] }],
                        [{ 'align': [] }],

                        ['clean'],                                         // remove formatting button
                    ]
                }
            }
        ),
        PipesModule

    ], exports: [
        PipesModule
    ]
})
export class StaffDiscussionsModule { }
