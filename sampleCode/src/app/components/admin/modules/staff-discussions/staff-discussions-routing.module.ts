import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StClassesComponent } from './st-classes/st-classes.component';
import { RouterComponent } from 'src/app/components/common/router/router.component';
import { StDiscussionsComponent } from './st-discussions/st-discussions.component';
import { StDiscussionManagerComponent } from './st-discussion-manager/st-discussion-manager.component';


const routes: Routes = [
    {
        path: '',
        component: RouterComponent,
        children: [
            {
                path: '',
                component: StClassesComponent
            }, {
                path: ':classId/discussions',
                component: RouterComponent,
                children: [
                    {
                        path: '',
                        component: StDiscussionsComponent
                    }, {
                        path: ':qId/p-view',
                        component: StDiscussionManagerComponent
                    }, {
                        path: ':qId/s-view',
                        component: StDiscussionManagerComponent
                    }
                ]
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class StaffDiscussionsRoutingModule { }
