import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { DiscussionService } from 'src/app/services/user/discussion/discussion.service';
import { AuthService } from 'src/app/services/common/auth/auth.service';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ContainerService } from 'src/app/services/admin/container/container.service';

@Component({
    selector: 'app-st-discussion-manager',
    templateUrl: './st-discussion-manager.component.html',
    styleUrls: ['./st-discussion-manager.component.scss']
})
export class StDiscussionManagerComponent implements OnInit {


    fileURL = this.auth.getImageURL();

    constructor(
        private route: ActivatedRoute,
        private discussionService: DiscussionService,
        private router: Router,
        private auth: AuthService,
        private fb: FormBuilder,
        private snackBar: MatSnackBar,
        private containerService: ContainerService
    ) { }

    ngOnInit(): void {
        this.getUserId();
        this.getClassId();
        this.getCurrentRoute();
        this.showActiveMenu();
    }

    qId;
    getUserId() {
        this.route.paramMap.subscribe((params: ParamMap) => {
            this.qId = this.route.snapshot.paramMap.get('qId');
        });
    }

    classId;
    getClassId() {
        this.route.parent.paramMap.subscribe((params: ParamMap) => {
            this.classId = this.route.parent.snapshot.paramMap.get('classId');
        });
    }

    showActiveMenu() {
        if (this.containerService.currentRouteValue !== 'staffDiscussionsRoute') {
            this.containerService.setActiveRoute('staffDiscussionsRoute')
        }
    }



    currentRoute;
    title;
    getCurrentRoute() {
        if (this.router.url.includes('/s-view')) {
            this.currentRoute = 'studentQA';
            this.title = "STUDENTS QUESTION";
            this.getStudentQA(this.qId);
            this.initializeStudentReplyForm();
        } else if (this.router.url.includes('/p-view')) {
            this.currentRoute = 'parentQA';
            this.title = "PARENTS QUESTION";
            this.getParentQA(this.qId);
            this.initializeParentReplyForm();
        }
    }


    studentQA = [];
    studentQALoaded = false;
    getStudentQA(studentId) {
        this.discussionService.getStudentQA(studentId).subscribe(
            res => {
                this.studentQALoaded = true;
                this.studentQA = res['data'][0];
            },
            err => {
                this.studentQALoaded = true;
                this.studentQA = [];
            }
        );

    }

    parentQA = [];
    parentQALoaded = false;
    getParentQA(parentId) {
        this.discussionService.getParentQA(parentId).subscribe(
            res => {
                this.parentQALoaded = true;
                this.parentQA = res['data'][0];
            },
            err => {
                this.parentQALoaded = true;
                this.parentQA = [];
            }
        );

    }

    studentReplyForm;
    initializeStudentReplyForm() {
        this.studentReplyForm = this.fb.group({
            files: [],
            qaReply: ['', [
                Validators.required,
            ]],
        });
    }

    parentReplyForm;
    initializeParentReplyForm() {
        this.parentReplyForm = this.fb.group({
            files: [],
            qaReply: ['', [
                Validators.required,
            ]],
        });
    }


    imagePath;
    valueChanged = false;
    imgURL;
    onFileSelected(event, currentView) {
        const file = (event.target as HTMLInputElement).files[0];
        if (currentView === 'studentReply') {
            this.studentReplyForm.patchValue({
                files: file
            });
            this.studentReplyForm.get('files').updateValueAndValidity();
        } else if (currentView === 'parentReply') {
            this.parentReplyForm.patchValue({
                files: file
            });
            this.parentReplyForm.get('files').updateValueAndValidity();
        }
        this.valueChanged = true;
        const reader = new FileReader();
        this.imagePath = file;
        reader.readAsDataURL(file);
        reader.onload = (_event) => {
            this.imgURL = reader.result;
        };
    }



    submitted = false;
    addStudentReply() {
        if (this.studentReplyForm.valid) {
            this.submitted = true;
            const formData: any = new FormData();
            formData.append('qa_reply  ', this.studentReplyForm.get('qaReply').value);
            if (this.valueChanged === true) {
                formData.append('files', this.studentReplyForm.get('files').value);
            }

            this.discussionService.replyStudentQA(this.qId, formData).subscribe(
                res => {
                    this.snackBar.open('Reply created successfully.', null, { duration: 3500, panelClass: ['snackbar-success'] });
                    this.getStudentQA(this.qId);
                    setTimeout(() => {
                        this.submitted = false;
                    }, 500);

                }, err => {
                    this.submitted = false;
                    this.snackBar.open(err.message, null, { duration: 3500, panelClass: ['snackbar-danger'] });
                }
            )
        }
    }



    addParentReply() {
        if (this.parentReplyForm.valid) {
            this.submitted = true;
            const formData: any = new FormData();
            formData.append('qa_reply  ', this.parentReplyForm.get('qaReply').value);
            if (this.valueChanged === true) {
                formData.append('files', this.parentReplyForm.get('files').value);
            }

            this.discussionService.replyParentQA(this.qId, formData).subscribe(
                res => {
                    this.snackBar.open('Reply created successfully.', null, { duration: 3500, panelClass: ['snackbar-success'] });
                    this.getParentQA(this.qId);
                    setTimeout(() => {
                        this.submitted = false;
                    }, 500);

                }, err => {
                    this.submitted = false;
                    this.snackBar.open(err.message, null, { duration: 3500, panelClass: ['snackbar-danger'] });
                }
            )
        }
    }
}