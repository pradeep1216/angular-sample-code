import { Component, OnInit, ViewChild } from '@angular/core';
import { DiscussionService } from 'src/app/services/user/discussion/discussion.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { ContainerService } from 'src/app/services/admin/container/container.service';

@Component({
    selector: 'app-st-discussions',
    templateUrl: './st-discussions.component.html',
    styleUrls: ['./st-discussions.component.scss']
})
export class StDiscussionsComponent implements OnInit {


    @ViewChild(DataTableDirective, { static: false })
    dtElement: DataTableDirective;
    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<any> = new Subject();

    constructor(
        private discussionService: DiscussionService,
        private route: ActivatedRoute,
        private containerService: ContainerService
    ) { }

    ngOnInit(): void {
        this.getClassId();
        this.getStudentsQA(this.classId);
        this.showActiveMenu();
        this.getClassDetail();
    }

    chooseUser(userType) {
        if (userType === 'parent') {
            this.getParentsQA(this.classId);
        } else if (userType === 'student') {
            this.getStudentsQA(this.classId);
        }
    }
    showActiveMenu() {
        if (this.containerService.currentRouteValue !== 'staffDiscussionsRoute') {
            this.containerService.setActiveRoute('staffDiscussionsRoute')
        }
    }

    classId;
    getClassId() {
        this.route.paramMap.subscribe((params: ParamMap) => {
            this.classId = this.route.snapshot.paramMap.get('classId');
        });
    }

    studentQA = [];
    studentQAsloaded = false;
    getStudentsQA(classId) {
        this.discussionService.getAllStudentsQAAdmin(classId).subscribe(
            res => {
                this.studentQAsloaded = true;
                this.studentQA = res['data'];
            },
            err => {
                this.studentQAsloaded = true;
            }
        );
    }

    parentsQA = [];
    parentsQALoaded = false;
    getParentsQA(classId) {
        this.discussionService.getAllParentsQAAdmin(classId).subscribe(
            res => {
                this.parentsQALoaded = true;
                this.parentsQA = res['data'];
            },
            err => {
                this.parentsQALoaded = true;
            }
        );
    }


    ngAfterViewInit(): void {
        this.dtTrigger.next();
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
    }

    rerender(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table `first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            this.dtTrigger.next();
        });
    }

    classDetail;
    getClassDetail() {
        this.classDetail = this.containerService.getClassDetailAdmin();
    }
}
