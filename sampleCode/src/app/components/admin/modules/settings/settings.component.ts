import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/admin/users/user.service';
import { AuthService } from 'src/app/services/common/auth/auth.service';
import { ContainerService } from 'src/app/services/admin/container/container.service';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

    user;
    userRole;
    passwordUpdateForm;
    submitted = false;
    data;
    dataLoaded = false;

    constructor(
        private userService: UserService,
        private auth: AuthService,
        private containerService: ContainerService,
        private fb: FormBuilder,
        private snackBar: MatSnackBar
    ) { }

    ngOnInit(): void {
        this.getUserDetails();
        this.getUserType();
        this.containerService.setActiveRoute('settingsRoute');
    }

    getUserDetails() {
        this.userService.getUser(this.auth.getLoggedInUserId())

        this.userService.getUser(this.auth.getLoggedInUserId()).subscribe(
            res => {
                this.user = res['data'][0];
                this.dataLoaded = true;
            },
            err => {
            }
        );
    }

    getUserType() {
        this.userRole = this.auth.currentUserValue;
        if (this.userRole === 'SA' || this.userRole === 'A') {
            this.initializeSettingsForm()
        }
    }

    initializeSettingsForm() {
        this.passwordUpdateForm = this.fb.group({
            newPassword: ['', [
                Validators.required,
            ]],
            rePassword: ['', [
                Validators.required,
            ]]
        });
    }

    changePassword() {
        this.submitted = true;
        if (this.passwordUpdateForm.valid) {
            if (this.passwordUpdateForm.get('newPassword').value === this.passwordUpdateForm.get('rePassword').value) {

                this.data = {
                    new_password: this.passwordUpdateForm.get('newPassword').value,
                    user_id: this.auth.getLoggedInUserId()
                }

                this.userService.updatePassword(this.data).subscribe(
                    res => {
                        this.snackBar.open('Password updated successfully.', null, { duration: 3500, panelClass: ['snackbar-success'] });
                        this.submitted = false;
                        this.passwordUpdateForm.reset();
                    }, err => {
                        this.snackBar.open(err.message, null, { duration: 3500, panelClass: ['snackbar-danger'] });
                        this.submitted = false;

                    }
                )

            } else {
                this.submitted = false;
                this.snackBar.open('Passwords do not match.', null, { duration: 3500, panelClass: ['snackbar-danger'] });
            }
        } else {
            this.snackBar.open('Invalid entry.', null, { duration: 3500, panelClass: ['snackbar-danger'] });
        }
    }

}
