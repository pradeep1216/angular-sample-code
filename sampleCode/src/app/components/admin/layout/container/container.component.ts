import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthService } from '../../../../services/common/auth/auth.service';
import { ContainerService } from '../../../../services/admin/container/container.service';
import { LoginService } from 'src/app/services/common/login/login.service';
import { MatDrawer } from '@angular/material/sidenav';

@Component({
    selector: 'app-container',
    templateUrl: './container.component.html',
    styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit {

    isExpanded;
    showUserMenu = false;
    userType;
    currentRoute;

    isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
        .pipe(
            map(result => result.matches),
            shareReplay()
        );

    constructor(
        private breakpointObserver: BreakpointObserver,
        private router: Router,
        private auth: AuthService,
        private containerService: ContainerService,
        private loginService: LoginService
    ) { }

    ngOnInit() {
        this.goToUsers();
        this.getUserType();
        this.getUserName();
        this.checkLoggedInUser();
    }

    ngAfterViewInit() {
        setTimeout(() => {
            this.containerService.activeRoute.subscribe(currRoute => {
                this.currentRoute = currRoute;
            });
        });

    }

    checkLoggedInUser() {
        if (!this.auth.getLoggedInUserId()) {
            this.router.navigateByUrl('/admin/login')
        }
    }

    getUserType() {
        this.userType = this.auth.getUserDetails();
    }

    goToUsers() {
        if (this.router.url.includes('/users')) {
            this.showUserMenu = true
        }
    }

    evt;
    goToMenu(path, destinationRoute) {
        this.router.navigateByUrl(path);
        if (destinationRoute === 'homeRoute') {
            this.containerService.setActiveRoute('homeRoute');
        } else if (destinationRoute === 'classRoute') {
            this.containerService.setActiveRoute('classroomRoute');
        } else if (destinationRoute === 'usersRoute') {
            if (this.showUserMenu == true) {
                this.showUserMenu = false;
            } else {
                this.showUserMenu = true
            }
            this.containerService.setActiveRoute('usersRoute');
        } else if (destinationRoute === 'userStaffsRoute') {
            this.showUserMenu = true;
            this.containerService.setActiveRoute('staffsRoute');
        } else if (destinationRoute === 'userStudentsRoute') {
            this.showUserMenu = true;
            this.containerService.setActiveRoute('studentsRoute');
        } else if (destinationRoute === 'userParentsRoute') {
            this.showUserMenu = true;
            this.containerService.setActiveRoute('parentsRoute');
        } else if (destinationRoute === 'settingsRoute') {
            this.containerService.setActiveRoute('settingsRoute');
        }
        else if (destinationRoute === 'stafftimetableRoute') {
            this.containerService.setActiveRoute('stafftimetableRoute');
        }
        else if (destinationRoute === 'staffAssignmentRoute') {
            this.containerService.setActiveRoute('staffAssignmentRoute');
        }
        else if (destinationRoute === 'staffUnitTestsRoute') {
            this.containerService.setActiveRoute('staffUnitTestsRoute');
        }
        else if (destinationRoute === 'staffDiscussionsRoute') {
            this.containerService.setActiveRoute('staffDiscussionsRoute');
        }
        else if (destinationRoute === 'classManagerRoute') {
            this.containerService.setActiveRoute('classManagerRoute');
        } else if (destinationRoute === 'staffAttendanceRoute') {
            this.containerService.setActiveRoute('staffAttendanceRoute');
        }
        this.isHandset$.subscribe(event => {
            this.evt = event
        })
        if (this.evt) {
            this.drawer.close();
        }
    }

    logout() {
        this.loginService.logout().subscribe(
            res => {
                this.router.navigate(['/admin/login'])
            },
            err => {
            }
        );
    }

    userName = '';
    getUserName() {
        this.userName = this.auth.getProfileDetails();
    }

    @ViewChild('drawer') drawer: MatDrawer;
}
