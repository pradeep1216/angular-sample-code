import { Component, OnInit } from '@angular/core';
import { TimeTableService } from 'src/app/services/admin/timeTable/time-table.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ContainerService } from 'src/app/services/admin/container/container.service';
import { AttendanceService } from 'src/app/services/common/attendance/attendance.service';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from 'src/app/services/common/auth/auth.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { FileService } from 'src/app/services/common/file/file.service';

@Component({
    selector: 'app-request',
    templateUrl: './request.component.html',
    styleUrls: ['./request.component.scss']
})
export class RequestComponent implements OnInit {

    constructor(
        private timeTableService: TimeTableService,
        private route: ActivatedRoute,
        private containerService: ContainerService,
        private attendanceService: AttendanceService,
        private fb: FormBuilder,
        private auth: AuthService,
        private snackBar: MatSnackBar,
        private deviceService: DeviceDetectorService,
        private fileService: FileService
    ) { }

    ngOnInit(): void {
        this.showActiveMenu();
        this.getIds();
        this.epicFunction();
    }

    showActiveMenu() {
        if (this.containerService.currentRouteValue !== 'studentTimetableRoute') {
            this.containerService.setActiveRoute('studentTimetableRoute')
        }
    }

    sessionId;
    fileId;
    getIds() {

        this.route.paramMap.subscribe((params: ParamMap) => {
            this.sessionId = this.route.snapshot.paramMap.get('sessionId');
        });

        this.route.paramMap.subscribe((params: ParamMap) => {
            this.fileId = this.route.snapshot.paramMap.get('fileId');
        });
        this.getUpload(this.fileId);

    }
    mediaURL = this.auth.getImageURL();


    upload;
    uploadLoaded = false;
    filePath;
    audioPath;
    documentPath;
    s3active = false;
    getUpload(fileId) {
        this.timeTableService.getUpload(fileId).subscribe(
            res => {
                this.uploadLoaded = true;
                this.upload = res['data'][0];

                if (this.auth.setServerFileDate() < this.fileService.dateFormter(this.upload['uploaded_date'])) {
                    this.s3active = true;
                    this.audioPath = this.upload['audio']
                    this.documentPath = this.upload['document']
                } else {
                    this.s3active = false;
                    this.audioPath = this.mediaURL + this.upload['audio']
                    this.documentPath = this.mediaURL + this.upload['document']
                }
            },
            err => {
                this.uploadLoaded = true;
            }
        );
    }

    redirectStatus = false;
    deviceInfo = null;
    epicFunction() {
        this.deviceInfo = this.deviceService.getDeviceInfo();
        if (this.deviceInfo['browser'] === 'Chrome' && this.deviceInfo['os'] === 'Windows') {
            this.redirectStatus = true;
        } else {
            this.redirectStatus = false;
        }
    }

    counter = 0;
    playVideo() {
        this.counter++;
    }

    onRightClick(event) {
        event.preventDefault();
    }

}
