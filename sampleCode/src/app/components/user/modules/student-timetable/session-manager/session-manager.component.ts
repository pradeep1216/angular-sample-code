import { Component, OnInit } from '@angular/core';
import { TimeTableService } from 'src/app/services/admin/timeTable/time-table.service';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ContainerService } from 'src/app/services/admin/container/container.service';
import { AttendanceService } from 'src/app/services/common/attendance/attendance.service';
import { FormBuilder, Validators } from '@angular/forms';
import { AsyncResponse } from 'src/app/interfaces/async-response';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
    selector: 'app-session-manager',
    templateUrl: './session-manager.component.html',
    styleUrls: ['./session-manager.component.scss']
})
export class SessionManagerComponent implements OnInit {

    constructor(
        private timeTableService: TimeTableService,
        private route: ActivatedRoute,
        private containerService: ContainerService,
        private attendanceService: AttendanceService,
        private fb: FormBuilder,
        private snackBar: MatSnackBar,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.showActiveMenu();
        this.getStudentAttendance();
        this.getIds();
        this.getCurrentTime();
        this.getTodayDate();
    }


    currentTime;
    getCurrentTime() {
        let d = new Date(); // for now

        let h = d.getHours();
        let hStr = JSON.stringify(h)
        if (h < 10) {
            hStr = '0' + hStr;
        }
        let m = d.getMinutes();
        let mStr = JSON.stringify(m)
        if (m < 10) {
            mStr = '0' + mStr;
        }
        let s = d.getSeconds();
        let sStr = JSON.stringify(s)
        if (s < 10) {
            sStr = '0' + sStr;
        }

        this.currentTime = hStr + ':' + mStr + ':' + sStr;
        return this.currentTime
    }

    currentDate;
    getTodayDate() {
        let d = new Date();
        let date = d.getDate();
        let dateStr = JSON.stringify(date)
        if (date < 10) {
            dateStr = '0' + dateStr;
        }

        let month = d.getMonth() + 1; // Since getMonth() returns month from 0-11 not 1-12
        let monthStr = JSON.stringify(month)
        if (month < 10) {
            monthStr = '0' + monthStr;
        }

        let year = d.getFullYear();
        this.currentDate = year + '-' + monthStr + '-' + dateStr;
    }

    showActiveMenu() {
        if (this.containerService.currentRouteValue !== 'studentTimetableRoute') {
            this.containerService.setActiveRoute('studentTimetableRoute')
        }
    }

    sessionId;
    fileId;
    getIds() {

        this.route.paramMap.subscribe((params: ParamMap) => {
            this.sessionId = this.route.snapshot.paramMap.get('sessionId');
        });

        this.route.paramMap.subscribe((params: ParamMap) => {
            this.fileId = this.route.snapshot.paramMap.get('fileId');
        });
        this.getUpload(this.fileId);
        this.getFileRequest(this.fileId);

    }

    fileRequest;
    fileRequestLoaded = false;
    getFileRequest(fileId) {
        this.timeTableService.getVideoRequestFileId(fileId).subscribe(
            res => {
                this.fileRequestLoaded = true;
                if (res['data'] === false)
                    this.fileRequest = res['data'];
                else
                    this.fileRequest = res['data'][0];
            },
            err => {
                this.fileRequestLoaded = true;
            }
        );
    }

    upload;
    uploadLoaded = false;
    getUpload(fileId) {
        this.timeTableService.getUpload(fileId).subscribe(
            res => {
                this.uploadLoaded = true;
                this.upload = res['data'][0];
                if (this.currentTime < this.upload['end_time']) {
                    this.snackBar.open('Unable to request video now.', null, { duration: 3500, panelClass: ['snackbar-danger'] });
                    this.router.navigateByUrl('/user/home')
                }
                this.initializeRequestForm(this.upload.classes, this.upload.subject, this.upload.staff, this.upload.file_id)
                this.getVideoLog();
            },
            err => {
                this.uploadLoaded = true;
            }
        );
    }

    videoLog;
    videoLogLoaded = false;
    getVideoLog() {
        this.timeTableService.getVideoLogStudent(this.fileId).subscribe(
            res => {
                this.videoLog = res['data'];
                this.videoLogLoaded = true;
            },
            err => {
                this.videoLogLoaded = true;
            }
        );
    }


    attendance;
    attendanceLoaded = false;
    getStudentAttendance() {
        this.attendanceService.getStudentCheckIn().subscribe(
            res => {
                this.attendance = res['action'];
                this.attendanceLoaded = true;
            },
            err => {
                this.attendanceLoaded = true;
            }
        );
    }

    addRequestForm;
    initializeRequestForm(classId, subjectId, staffId, fileId) {
        this.addRequestForm = this.fb.group({
            classId: ['', [
                Validators.required,
            ]],
            subjectId: ['', [
                Validators.required,
            ]],
            staffId: ['', [
                Validators.required,
            ]],
            fileId: ['', [
                Validators.required,
            ]],
            requestReason: ['', [
                Validators.required,
            ]],
        });
        this.addRequestForm.patchValue({
            classId: classId,
            subjectId: subjectId,
            staffId: staffId,
            fileId: fileId
        })
    }

    data;
    submitted = false;
    addRequest() {
        this.submitted = true;
        if (this.addRequestForm.valid) {
            if (this.currentTime > this.upload.end_time || this.currentDate > this.upload.display_date) {
                this.data = {
                    'class_id': this.addRequestForm.get('classId').value,
                    'subject_id': this.addRequestForm.get('subjectId').value,
                    'staff_id': this.addRequestForm.get('staffId').value,
                    'file_id': this.addRequestForm.get('fileId').value,
                    'video_request_reason': this.addRequestForm.get('requestReason').value
                }
                const formData: any = new FormData();
                formData.append('subject_id', this.addRequestForm.get('subjectId').value);
                formData.append('staff_id', this.addRequestForm.get('staffId').value);
                formData.append('file_id', this.addRequestForm.get('fileId').value);
                formData.append('video_request_reason', this.addRequestForm.get('requestReason').value);

                this.timeTableService.addVideoRequest(this.data).subscribe(
                    (res: AsyncResponse) => {
                        this.snackBar.open('Request added Successfully.', null, { duration: 3500, panelClass: ['snackbar-success'] });
                        this.getFileRequest(this.fileId);
                    }, (err: AsyncResponse) => {
                        this.snackBar.open(err.message, null, { duration: 3500, panelClass: ['snackbar-danger'] });
                        this.submitted = false;
                    });
            } else {
                this.snackBar.open('Unable to submit request now.', null, { duration: 3500, panelClass: ['snackbar-danger'] });
                this.submitted = false;
            }
        }
    }

}
