import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TimetableComponent } from './timetable/timetable.component';
import { SessionsComponent } from './sessions/sessions.component';
import { SessionManagerComponent } from './session-manager/session-manager.component';
import { RequestComponent } from './request/request.component';


const routes: Routes = [
    {
        path: '',
        component: TimetableComponent
    }, {
        path: ':sessionId',
        component: SessionsComponent
    }, {
        path: ':sessionId/:fileId/details',
        component: SessionManagerComponent
    }, {
        path: ':sessionId/:fileId/details/:requestId',
        component: RequestComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class StudentTimetableRoutingModule { }
