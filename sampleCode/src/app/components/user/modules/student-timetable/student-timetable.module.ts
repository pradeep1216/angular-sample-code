import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StudentTimetableRoutingModule } from './student-timetable-routing.module';
import { TimetableComponent } from './timetable/timetable.component';
import { SessionsComponent } from './sessions/sessions.component';
import { SessionManagerComponent } from './session-manager/session-manager.component';
import { DataTablesModule } from 'angular-datatables';
import { PipesModule } from 'src/app/modules/pipes.module';
import { ReactiveFormsModule } from '@angular/forms';
import { RequestComponent } from './request/request.component';
import { VgCoreModule, } from '@videogular/ngx-videogular/core';

import { VgControlsModule } from '@videogular/ngx-videogular/controls';
import { VgOverlayPlayModule } from '@videogular/ngx-videogular/overlay-play';
import { VgBufferingModule } from '@videogular/ngx-videogular/buffering';

@NgModule({
    declarations: [TimetableComponent, SessionsComponent, SessionManagerComponent, RequestComponent],
    imports: [
        CommonModule,
        StudentTimetableRoutingModule,
        DataTablesModule,
        PipesModule,
        ReactiveFormsModule,
        VgCoreModule,
        VgControlsModule,
        VgOverlayPlayModule,
        VgBufferingModule,
    ], providers: [
        PipesModule
    ]
})
export class StudentTimetableModule { }
