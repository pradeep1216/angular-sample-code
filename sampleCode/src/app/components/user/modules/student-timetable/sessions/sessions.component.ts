import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { TimeTableService } from 'src/app/services/admin/timeTable/time-table.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ContainerService } from 'src/app/services/admin/container/container.service';

@Component({
    selector: 'app-sessions',
    templateUrl: './sessions.component.html',
    styleUrls: ['./sessions.component.scss']
})
export class SessionsComponent implements OnInit {

    modalRef: BsModalRef;

    @ViewChild(DataTableDirective, { static: false })
    dtElement: DataTableDirective;
    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<any> = new Subject();

    constructor(
        private route: ActivatedRoute,
        private timeTableService: TimeTableService,
        private modalService: BsModalService,
        private containerService: ContainerService
    ) { }

    ngOnInit(): void {
        this.getIds();
        this.showActiveMenu();
    }


    showActiveMenu() {
        if (this.containerService.currentRouteValue !== 'studentTimetableRoute') {
            this.containerService.setActiveRoute('studentTimetableRoute')
        }
    }



    sessionId;
    classId;
    getIds() {

        this.route.paramMap.subscribe((params: ParamMap) => {
            this.sessionId = this.route.snapshot.paramMap.get('sessionId');
        });
        this.route.paramMap.subscribe((params: ParamMap) => {
            this.classId = this.route.parent.snapshot.paramMap.get('classId');
        });
        this.getAllSessions(this.sessionId);
    }


    sessions;
    uploads = [];
    sessionsLoaded = false;
    getAllSessions(sessionId) {
        this.timeTableService.getSessionRelatedDetails(sessionId).subscribe(
            res => {
                this.sessionsLoaded = true;
                this.sessions = res['data'][0];
                this.uploads = this.sessions['timetable_upload'];
                this.rerender();
            },
            err => {
                this.sessionsLoaded = true;
            }
        );
    }

    ngAfterViewInit(): void {
        this.dtTrigger.next();
    }

    ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
    }

    rerender(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table `first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            this.dtTrigger.next();
        });
    }

    delFileId;
    openModal(template: TemplateRef<any>, timetableId) {
        this.modalRef = this.modalService.show(template);
        this.delFileId = timetableId;
    }

    closeModal() {
        const modalHeader = document.getElementsByClassName('modal-header');
        if (modalHeader && modalHeader.length > 0) {
            // Get the close button in the modal header
            const closeButton: any = modalHeader[0].getElementsByTagName('button');
            if (closeButton && closeButton.length > 0) {
                // simulate click on close button
                closeButton[0].click();
            }
        }
    }


}
