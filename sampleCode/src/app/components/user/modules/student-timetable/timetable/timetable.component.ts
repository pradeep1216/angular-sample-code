import { Component, OnInit, } from '@angular/core';
import { TimeTableService } from 'src/app/services/admin/timeTable/time-table.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ClassroomService } from 'src/app/services/admin/classroom/classroom.service';
import { ContainerService } from 'src/app/services/admin/container/container.service';

@Component({
    selector: 'app-timetable',
    templateUrl: './timetable.component.html',
    styleUrls: ['./timetable.component.scss']
})
export class TimetableComponent implements OnInit {

    constructor(
        private timeTableService: TimeTableService,
        private containerService: ContainerService

    ) { }

    ngOnInit(): void {
        this.showActiveMenu();
        this.getTimeTable();
        this.getTodayDay();
    }


    showActiveMenu() {
        if (this.containerService.currentRouteValue !== 'studentTimetableRoute') {
            this.containerService.setActiveRoute('studentTimetableRoute')
        }
    }



    timeTable = [];
    dataLoaded = false;
    getTimeTable() {
        this.timeTableService.getAllPeriodsStudent().subscribe(
            res => {
                this.timeTable = res['data'];
                this.updateDay(this.timeTable)
                this.dataLoaded = true;
            },
            err => {
                this.dataLoaded = true;
            }
        );
    }

    updateDay(data) {
        for (let i = 0; i < data.length; i++) {
            for (let j = 0; j < this.days.length; j++) {
                if (data[i].display_day === this.days[j]['day']) {
                    this.days[j].periodDetails.push(data[i])
                }
            }
        }
    }


    days = [
        {
            day: 'MON',
            name: 'Monday',
            dayNum: 1,
            periodDetails: []
        }, {
            day: 'TUE',
            name: 'Tuesday',
            dayNum: 2,
            periodDetails: []
        }, {
            day: 'WED',
            name: 'Wednesday',
            dayNum: 3,
            periodDetails: []
        }, {
            day: 'THU',
            name: 'Thursday',
            dayNum: 4,
            periodDetails: []
        }, {
            day: 'FRI',
            name: 'Friday',
            dayNum: 5,
            periodDetails: []
        }, {
            day: 'SAT',
            name: 'Saturday',
            dayNum: 6,
            periodDetails: []
        }, {
            day: 'SUN',
            name: 'Sunday',
            dayNum: 0,
            periodDetails: []
        }
    ]


    activeDay;
    currentDay;
    getTodayDay() {
        let today = new Date();
        this.currentDay = today.getDay();
        this.activeDay = this.currentDay;
    }

    showCurrentTimeTable(clickedDay) {
        this.activeDay = clickedDay;
    }
}
