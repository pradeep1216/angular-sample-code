import { Component, OnInit } from '@angular/core';
import { ContainerService } from 'src/app/services/admin/container/container.service';
import { UserService } from 'src/app/services/admin/users/user.service';
import { AuthService } from 'src/app/services/common/auth/auth.service';

@Component({
    selector: 'app-user-settings',
    templateUrl: './user-settings.component.html',
    styleUrls: ['./user-settings.component.scss']
})
export class UserSettingsComponent implements OnInit {


    constructor(
        private containerService: ContainerService,
        private userService: UserService,
        private auth: AuthService,
    ) { }

    ngOnInit(): void {
        this.getUserDetails();
        this.showActiveMenu();
    }

    user;
    userLoaded = false;
    getUserDetails() {
        this.userService.getUser(this.auth.getLoggedInUserId())

        this.userService.getUser(this.auth.getLoggedInUserId()).subscribe(
            res => {
                this.user = res['data'][0];
                this.userLoaded = true;
            },
            err => {
                this.userLoaded = true;
            }
        );
    }
    showActiveMenu() {
        if (this.containerService.currentRouteValue !== 'userSettingsRoute') {
            this.containerService.setActiveRoute('userSettingsRoute')
        }
    }

}
