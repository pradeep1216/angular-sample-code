import { Component, OnInit } from '@angular/core';
import { TimeTableService } from 'src/app/services/admin/timeTable/time-table.service';
import { UserService } from 'src/app/services/admin/users/user.service';
import { AuthService } from 'src/app/services/common/auth/auth.service';
import { ContainerService } from 'src/app/services/admin/container/container.service';
import { AttendanceService } from 'src/app/services/common/attendance/attendance.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DeviceDetectorService } from 'ngx-device-detector';
import { Router } from '@angular/router';

@Component({
    selector: 'app-user-dashboard-view',
    templateUrl: './user-dashboard-view.component.html',
    styleUrls: ['./user-dashboard-view.component.scss']
})
export class UserDashboardViewComponent implements OnInit {


    today = new Date().getDay();
    dataLoaded = false;
    mediaURL = this.auth.getImageURL();
    constructor(
        private timeTableService: TimeTableService,
        private userService: UserService,
        private auth: AuthService,
        private containerService: ContainerService,
        private attendanceService: AttendanceService,
        private snackBar: MatSnackBar,
        private deviceService: DeviceDetectorService,
        private router: Router
    ) { }

    ngOnInit(): void {
        this.getDay();
        this.getCurrentUser();
        this.showActiveMenu();
    }

    getStudentDashboardDetails() {
        this.getUserCount();
        this.getTodaysVideos();
    }

    currentUser;
    getCurrentUser() {
        this.currentUser = this.auth.currentUserValue
        if (this.currentUser === 'ST') {
            this.getStudentAttendance();

        } else if (this.currentUser === 'P') {
            this.getUserCount();
        }
    }
    showActiveMenu() {
        if (this.containerService.currentRouteValue !== 'userHomeRoute') {
            this.containerService.setActiveRoute('userHomeRoute')
        }
    }

    getDay() {
        let dayName;
        if (this.today == 0) {
            dayName = 'SUN'
        } else if (this.today == 1) {
            dayName = 'MON'
        } else if (this.today == 2) {
            dayName = 'TUE'
        } else if (this.today == 3) {
            dayName = 'WED'
        } else if (this.today == 4) {
            dayName = 'THU'
        } else if (this.today == 5) {
            dayName = 'FRI'
        } else if (this.today == 6) {
            dayName = 'SAT'
        }
        return dayName;
    }

    studentDetails;
    getTodaysVideos() {
        this.timeTableService.getTodayVideos(this.getDay()).subscribe(
            res => {
                this.studentDetails = res['data']
            },
            err => {
            }
        );
    }

    userCount;
    getUserCount() {
        this.userService.getUserCount().subscribe(
            res => {
                this.dataLoaded = true;
                this.userCount = res['data']
            },
            err => {
                this.dataLoaded = true;
            }
        );
    }

    attendance;
    attendanceLoaded = false;
    getStudentAttendance() {
        this.attendanceService.getStudentCheckIn().subscribe(
            res => {
                this.attendance = res['action'];
                this.attendanceLoaded = true;
                if (this.attendance === true || this.attendance === 'leave') {
                    this.getStudentDashboardDetails();
                }
            },
            err => {
                this.attendanceLoaded = true;
            }
        );
    }

    submitted = false;
    markAttendance() {
        this.submitted = true;
        this.attendanceService.checkInStudent().subscribe(
            res => {
                this.submitted = false;
                this.getStudentAttendance();
                this.snackBar.open('Attendance marked successfully.', null, { duration: 3500, panelClass: ['snackbar-success'] });
            }, err => {
                this.snackBar.open(err.message, null, { duration: 3500, panelClass: ['snackbar-danger'] });
                this.submitted = false;
            }
        )
    }

    deviceInfo = null;
    isMobile = null;
    isTablet = null;
    isDesktopDevice = null;
    deviceDetails;
    epicFunction() {
        this.deviceInfo = this.deviceService.getDeviceInfo();
        this.isMobile = this.deviceService.isMobile();
        this.isTablet = this.deviceService.isTablet();
        this.isDesktopDevice = this.deviceService.isDesktop();
        if (this.isDesktopDevice) {
            this.deviceDetails = {
                'deviceType': 'desktop',
                'browser': this.deviceInfo['browser'],
                'browser_version': this.deviceInfo['browser_version'],
                'os': this.deviceInfo['os'],
            }
        } else if (this.isTablet) {
            this.deviceDetails = {
                'deviceType': 'tablet',
                'browser': this.deviceInfo['browser'],
                'browser_version': this.deviceInfo['browser_version'],
                'os': this.deviceInfo['os'],
            }
        } else if (this.isMobile) {
            this.deviceDetails = {
                'deviceType': 'mobile',
                'browser': this.deviceInfo['browser'],
                'browser_version': this.deviceInfo['browser_version'],
                'os': this.deviceInfo['os'],
            }
        }

        return this.deviceDetails;
    }

    goToURL(upload, startTime, endTime) {
        let currentTime = this.getCurrentTime();
        let redirectURL = 'user/' + upload.file_id + '/live'
        if (currentTime > startTime && currentTime < endTime) {
            this.router.navigate([redirectURL]);

        } else {
            this.snackBar.open('Unable to join class now.', null, { duration: 3500, panelClass: ['snackbar-danger'] });
        }
    }


    goToVideo(upload, startTime, endTime) {
        let currentTime = this.getCurrentTime();
        if (currentTime > startTime && currentTime < endTime) {
            let redirectURL = '/user/' + upload.file_id + '/videos'
            this.router.navigateByUrl(redirectURL);
        } else {
            this.snackBar.open('Unable to join class now.', null, { duration: 3500, panelClass: ['snackbar-danger'] });
        }
    }

    data;
    setVideoLog(fileId) {
        this.data = {
            'file_id': fileId
        }
        this.timeTableService.addVideoLog(this.data).subscribe(
            res => {
                console.log('Success');
            }, err => {
                this.snackBar.open(err.message, null, { duration: 3500, panelClass: ['snackbar-danger'] });
            }
        )
    }

    currentTime
    getCurrentTime() {
        let d = new Date(); // for now

        let h = d.getHours();
        let hStr = JSON.stringify(h)
        if (h < 10) {
            hStr = '0' + hStr;
        }
        let m = d.getMinutes();
        let mStr = JSON.stringify(m)
        if (m < 10) {
            mStr = '0' + mStr;
        }
        let s = d.getSeconds();
        let sStr = JSON.stringify(s)
        if (s < 10) {
            sStr = '0' + sStr;
        }

        this.currentTime = hStr + ':' + mStr + ':' + sStr;
        return this.currentTime
    }


}
