import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthService } from '../../../../services/common/auth/auth.service';
import { ContainerService } from '../../../../services/admin/container/container.service';
import { LoginService } from 'src/app/services/common/login/login.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TimeTableService } from 'src/app/services/admin/timeTable/time-table.service';
import { UserService } from 'src/app/services/admin/users/user.service';
import { AttendanceService } from 'src/app/services/common/attendance/attendance.service';
import { MatDrawer } from '@angular/material/sidenav';

@Component({
    selector: 'app-user-container',
    templateUrl: './user-container.component.html',
    styleUrls: ['./user-container.component.scss']
})
export class UserContainerComponent implements OnInit {

    isExpanded;
    showUserMenu = false;
    userType;
    currentRoute;
    dataLoaded = false;
    isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
        .pipe(
            map(result => result.matches),
            shareReplay()
        );

    constructor(
        private breakpointObserver: BreakpointObserver,
        private router: Router,
        private auth: AuthService,
        private containerService: ContainerService,
        private attendanceService: AttendanceService,
        private loginService: LoginService
    ) { }


    today = new Date().getDay();

    ngOnInit() {
        this.goToUsers();
        this.getUserType();
        this.getUserName();
        this.checkLoggedInUser();
    }

    checkLoggedInUser() {
        if (!this.auth.getLoggedInUserId()) {
            this.router.navigateByUrl('/user/login')
        }
    }
    ngAfterViewInit() {
        setTimeout(() => {
            this.containerService.activeRoute.subscribe(currRoute => {
                this.currentRoute = currRoute;
            });
        });
    }

    getUserType() {
        this.userType = this.auth.currentUserValue;
        if (this.userType === 'ST') {
            this.getStudentAttendance();
        }
    }



    attendance;
    attendanceLoaded = false;
    getStudentAttendance() {
        this.attendanceService.getStudentCheckIn().subscribe(
            res => {
                this.attendance = res['data'];
                this.attendanceLoaded = true;
            },
            err => {
            }
        );
    }



    goToUsers() {
        if (this.router.url.includes('/users')) {
            this.showUserMenu = true
        }
    }

    evt
    goToMenu(path, destinationRoute) {
        this.router.navigateByUrl(path);
        if (destinationRoute === 'userHomeRoute') {
            this.containerService.setActiveRoute('userHomeRoute');
        } else if (destinationRoute === 'userSettingsRoute') {
            this.containerService.setActiveRoute('userSettingsRoute');
        }
        else if (destinationRoute === 'studentDiscussionsRoute') {
            this.containerService.setActiveRoute('studentDiscussionsRoute');
        } else if (destinationRoute === 'parentDiscussionsRoute') {
            this.containerService.setActiveRoute('parentDiscussionsRoute');
        }

        else if (destinationRoute === 'studentAssignmentRoute') {
            this.containerService.setActiveRoute('studentAssignmentRoute');
        }
        else if (destinationRoute === 'studentTestsRoute') {
            this.containerService.setActiveRoute('studentTestsRoute');
        } else if (destinationRoute === 'parentStudentsRoute') {
            this.containerService.setActiveRoute('parentStudentsRoute');
        }
        else if (destinationRoute === 'studentTimetableRoute') {
            this.containerService.setActiveRoute('studentTimetableRoute');
        } else if (destinationRoute === 'studentAttendanceRoute') {
            this.containerService.setActiveRoute('studentAttendanceRoute');
        } else if (destinationRoute === 'parentAttendanceRoute') {
            this.containerService.setActiveRoute('parentAttendanceRoute');
        }
        this.isHandset$.subscribe(event => {
            this.evt = event
        })
        if (this.evt) {
            this.drawer.close();
        }
    }

    logout() {
        this.loginService.logout().subscribe(
            res => {
                this.router.navigate(['/user/login'])
            },
            err => {
            }
        );
    }

    userName = '';
    getUserName() {
        this.userName = this.auth.getProfileDetails();
    }

    @ViewChild('drawer') drawer: MatDrawer;

}
