import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'classType'
})
export class ClassTypePipe implements PipeTransform {

    transform(value: any): unknown {
        let classType;
        if (value === 'R')
            classType = 'Recorded';
        else if (value === 'LC')
            classType = 'Live';

        return classType;
    }

}
