import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'fullDay'
})
export class FullDayPipe implements PipeTransform {

    transform(value: any): unknown {
        let day;
        if (value === 'MON')
            day = 'Monday';
        else if (value === 'TUE')
            day = 'Tuesday';
        else if (value === 'WED')
            day = 'Wednesday';
        else if (value === 'THU')
            day = 'Thursday';
        else if (value === 'FRI')
            day = 'Friday';
        else if (value === 'SAT')
            day = 'Saturday';
        else if (value === 'SUN')
            day = 'Sunday';
        return day;
    }

}
