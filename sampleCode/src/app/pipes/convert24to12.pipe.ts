import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'convert24to12'
})
export class Convert24to12Pipe implements PipeTransform {

    transform(value: any): string {
        var timeString = value;
        var H = +timeString.substr(0, 2);
        var h = (H % 12) || 12;
        var ampm = H < 12 ? "AM" : "PM";
        timeString = h + timeString.substr(2, 3) + ampm;
        return timeString;
    }
}
