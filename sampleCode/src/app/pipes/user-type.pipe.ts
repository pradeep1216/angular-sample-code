import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'userType'
})
export class UserTypePipe implements PipeTransform {

    transform(value: string): string {
        let userType;
        if (value === 'SA')
            userType = 'Super Admin';
        else if (value === 'A')
            userType = 'Admin';
        else if (value === 'SF')
            userType = 'Staff';
        else if (value === 'ST')
            userType = 'Student';
        else if (value === 'P')
            userType = 'Parent';

        return userType;
    }

}
