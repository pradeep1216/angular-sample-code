import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'staffType'
})
export class StaffTypePipe implements PipeTransform {

    transform(value: string): string {
        let staff;
        if (value === 'M')
            staff = 'Class Teacher';
        else if (value === 'NM')
            staff = 'Teacher';
        return staff;
    }

}
