export enum Roles {
    Admin = 'A',
    Parent = 'P',
    Staff = 'SF',
    Student = 'ST',
    SuperAdmin = 'SA'
}