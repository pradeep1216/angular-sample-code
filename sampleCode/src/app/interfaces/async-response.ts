export interface AsyncResponse {
    message: string;
    status: 'success' | 'failed' | 'error' | 'info' | 'progress';
    errors?: any;
    class?: any;
    action?: string;
    data?: any;
    star?: any;
    progressBar?: number;
    count?: any;
}
