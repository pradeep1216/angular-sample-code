import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class FileService {

    constructor() { }

    // FILE SIZE IN MB
    fileSize;



    dateFormter(date) {

        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');

    }


    hoursConvert12To24(time) {
        var hours = Number(time.match(/^(\d+)/)[1]);
        var minutes = Number(time.match(/:(\d+)/)[1]);
        var AMPM = time.match(/\s(.*)$/)[1];
        if (AMPM == "PM" && hours < 12) hours = hours + 12;
        if (AMPM == "AM" && hours == 12) hours = hours - 12;
        var sHours = hours.toString();
        var sMinutes = minutes.toString();
        if (hours < 10) sHours = "0" + sHours;
        if (minutes < 10) sMinutes = "0" + sMinutes;
        let retTime = sHours + ":" + sMinutes;
        return retTime;
    }

    hoursConvert24To12(value) {
        var timeString = value;
        var H = +timeString.substr(0, 2);
        var h = (H % 12) || 12;
        var ampm = H < 12 ? "am" : "pm";
        timeString = h + timeString.substr(2, 3) + ' ' + ampm;
        return timeString;
    }



    getDateTime(date, time) {
        date = this.dateFormter(date);
        time = this.hoursConvert12To24(time);
        let dateTime = date + 'T' + time
        return dateTime;
    }

    getDateTimeWAM(date, time) {
        date = this.dateFormter(date);
        let dateTime = date + 'T' + time
        return dateTime;
    }




    getFileType(file) {
        let fileType;
        switch (file) {
            case 'mp3audio':
                fileType = 'audio/mp3'
                return fileType;
            case 'mpegaudio':
                fileType = 'audio/mpeg'
                return fileType;
            case 'mp4video':
                fileType = 'video/mp4'
                return fileType;
            case 'pdfdocument':
                fileType = 'application/pdf'
                return fileType;
            case 'imagepng':
                fileType = 'image/png'
                return fileType;
            case 'imagejpg':
                fileType = 'image/jpg'
                return fileType;
            case 'imagejpeg':
                fileType = 'image/jpeg'
                return fileType;
            default:
                return 'Invalid entry';
        }
    }

    millisToMinutesAndSeconds(millis) {
        let minutes = Math.floor(millis / 60000);
        let seconds = ((millis % 60000) / 1000).toFixed(0);
        let time = minutes + ':' + seconds;
        let data = {
            time: time,
            minutes: minutes,
            seconds: seconds
        }
        return data;
    }

    minutesToMilliseconds(minutes) {
        let ms = minutes * 60000;
        return ms;
    }

    getToday() {
        var today = new Date();
        let currdate = today.getDate();
        var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        let currMonth = months[today.getMonth()]
        let currYear = today.getFullYear()
        return (currdate + '-' + currMonth + '-' + currYear)
    }
}
