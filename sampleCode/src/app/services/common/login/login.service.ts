import { Injectable } from '@angular/core';
import { AsyncResponse } from '../../../interfaces/async-response';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../common/auth/auth.service';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    constructor(
        private http: HttpClient,
        private auth: AuthService,
        private router: Router
    ) { }

    /*
    * Login User
    * @param object of userData and currentUser route
    * @return Promise<AsyncResponse>
    */
    login(data, currentRoute): Observable<AsyncResponse> {
        let APIpath;
        return new Observable(subscribe => {
            if (data && currentRoute) {
                this.auth.clearAllLocalStorage();
                if (currentRoute === 'admin') {
                    APIpath = 'admin/login/';
                } else if (currentRoute === 'user') {
                    APIpath = 'admin/login/student_parent/';
                }

                this.http.post<any>(this.auth.getBaseURL() + APIpath, data)
                    .subscribe(
                        res => {
                            this.auth.setToken(res['token']);
                            this.auth.setUserDetails(res['data'][0]['user_type']);
                            this.auth.setLoggedInUserId(res['data'][0]['user_id']);
                            this.auth.setUser(res['data'][0]['user_type']);
                            this.auth.setProfileDetails(res['data'][0]['username']);
                            subscribe.next({
                                status: 'success',
                                message: 'Successfully logged in.',
                                data: res['data']
                            });
                        },
                        error => {
                            subscribe.error({
                                status: 'failed',
                                message: error.error.error
                            });
                        }
                    );

            } else {
                subscribe.error({
                    status: 'failed',
                    message: 'Invalid Credentials.'
                });
            }
        });
    }

    /*
    * Logout User
    * @param object of userData and currentUser route
    * @return Promise<AsyncResponse>
    */
    logout(): Observable<AsyncResponse> {
        return new Observable(subscribe => {
            this.http.delete<any>(this.auth.getBaseURL() + 'user/login/logout/', this.auth.getHTTPHeader('token'))
                .subscribe(
                    res => {
                        this.auth.removeUserDetails();
                        this.auth.removeToken();
                        this.auth.removeLoggedInUserId();
                        this.auth.endUserRole();
                        this.auth.removeProfileDetails();
                        this.auth.clearAllLocalStorage();
                        subscribe.next({
                            status: 'success',
                            message: 'Successfully logged out.',
                            data: res['data']
                        });
                    },
                    error => {
                        if (this.router.url.includes('/admin/')) {
                            let redirectRoute = '/admin/login';
                            this.auth.clearAllLocalStorage();
                            this.router.navigate([redirectRoute]);
                        } else {
                            let redirectRoute = '/user/login';
                            this.auth.clearAllLocalStorage();
                            this.router.navigate([redirectRoute]);
                        }
                        subscribe.error({
                            status: 'failed',
                            message: error.error.msg
                        });
                    }
                );

        });
    }


}
