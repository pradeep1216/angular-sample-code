import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private auth: AuthService
    ) { }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.auth.currentUserValue;

        if (currentUser && this.auth.getUserDetails() && currentUser === this.auth.getUserDetails()) {

            if (route.data.roles && route.data.roles.indexOf(currentUser.role) === -1) {
                let n = route.data.roles.includes(currentUser)
                if (n) {

                    return true;
                } else {
                    this.router.navigateByUrl('/admin/access-forbidden');
                    return false;
                }
            }

        } else {
            this.router.navigateByUrl('/admin/login');
            return false;
        }
    }
}
