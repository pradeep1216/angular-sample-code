import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { FileService } from '../file/file.service';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    public currentUser: Observable<any>;
    public currentUserSubject = new BehaviorSubject('userRole');

    constructor(
        private http: HttpClient,
        private fileService: FileService
    ) {
        this.currentUserSubject = new BehaviorSubject<any>((localStorage.getItem('userDetails')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    /*
    * set behaviour subject based on the user role
    * @param userRole
    * @return void
    */
    setUser(role) {
        this.currentUserSubject.next(role);
    }

    endUserRole() {
        this.currentUserSubject.next('');
        this.currentUserSubject.complete();
    }

    /*
    * get behaviour subject based on the user role
    * @param void
    * @return userRole
    */
    public get currentUserValue(): any {
        return this.currentUserSubject.value;
    }

    /*
   	* get HTTPHeader 
   	* @param string tokenType - token , image or ''
   	* @return HTTP header
   	*/
    getHTTPHeader(tokenType) {
        let httpOptions;
        switch (tokenType) {
            case 'token':
                httpOptions = {
                    headers: new HttpHeaders({
                        'Authorization': 'Token ' + this.getToken(),
                        'Content-Type': 'application/json'
                    })
                }
                return httpOptions;
            case 'image':
                httpOptions = {
                    headers: new HttpHeaders({
                        'Authorization': 'Token ' + this.getToken(),
                    })
                }
                return httpOptions;
            default:
                httpOptions = {};
                return httpOptions;
        }
    }

    /*
    * get token from localStorage
    * @param void empty
    * @return loggedInUserId
    */
    getToken() {
        return localStorage.getItem('token');
    }

    /*
    * set token in localStorage
    * @param object token
    * @return void
    */
    setToken(token: string) {
        localStorage.setItem('token', token);
    }

    /*
    * Remove token in localStorage
    * @param object token
    * @return void
    */
    removeToken() {
        localStorage.removeItem('token');
    }


    /*
    * get userType from localStorage
    * @param void empty
    * @return loggedInUserId
    */
    getUserDetails() {
        let data = localStorage.getItem('userDetails');
        // this.setUser(data)
        return data
    }

    /*
    * set a userType in localStorage
    * @param object token
    * @return void
    */
    setUserDetails(userDetails: string) {
        localStorage.setItem('userDetails', userDetails);
    }

    /*
    * Remove userType in localStorage
    * @param object token
    * @return void
    */
    removeUserDetails() {
        localStorage.removeItem('userDetails');
    }

    /*
    * get LoggedInUser from localStorage
    * @param void empty
    * @return loggedInUserId
    */
    getLoggedInUserId() {
        let data = localStorage.getItem('loggedInUserId');
        return data
    }

    /*
    * set LoggedInUser in localStorage
    * @param object token
    * @return void
    */
    setLoggedInUserId(userId: string) {
        localStorage.setItem('loggedInUserId', userId);
    }

    /*
    * Remove LoggedInUser in localStorage
    * @param object token
    * @return void
    */
    removeLoggedInUserId() {
        localStorage.removeItem('loggedInUserId');
    }

    /*
    * get token from localStorage
    * @param void empty
    * @return loggedInUserId
    */
    getParentDetails() {
        let data = localStorage.getItem('parentDetails');
        return data
    }

    /*
    * set a token in localStorage
    * @param object token
    * @return void
    */
    setParentDetails(userDetails: string) {
        localStorage.setItem('parentDetails', userDetails);
    }

    deleteParentDetails() {
        localStorage.removeItem('parentDetails')
    }



    /*
   * get ProfileDetails from localStorage
   * @param void empty
   * @return ProfileDetails
   */
    getProfileDetails() {
        return localStorage.getItem('profileDetails');
    }

    /*
    * set ProfileDetails in localStorage
    * @param object ProfileDetails
    * @return void
    */
    setProfileDetails(details: string) {
        localStorage.setItem('profileDetails', details);
    }

    /*
    * Remove ProfileDetails in localStorage
    * @param void
    * @return void
    */
    removeProfileDetails() {
        localStorage.removeItem('profileDetails');
    }

    /*
   * get meetingId from localStorage
   * @param void empty
   * @return ProfileDetails
   */
    getMeetingId() {
        return localStorage.getItem('meetingId');
    }


    setMeetingId(details: string) {
        localStorage.setItem('meetingId', details);
    }

    removeMeetingId() {
        localStorage.removeItem('meetingId');
    }


    clearAllLocalStorage() {
        localStorage.clear();
    }


    getToday() {
        let today = new Date();
        let day = this.fileService.dateFormter(today)
        return day;
    }

    setFileEndPoint() {
        let mode = this.applicationMode();
        let endPoint;
        if (mode === 'dev') {
            endPoint = '';
        } else if (mode === 'live') {
            endPoint = '';
        } else if (mode === 'local') {
            endPoint = '';
        }


        return endPoint;
    }

    checkEndPoint(location) {
        if (location !== null && location.includes(this.setFileEndPoint())) {
            return location;
        } else if (location === null || location == '/media/null') {
            return null;
        } else {
            return this.getImageURL() + location;
        }
    }




    /*
    * get base backend URL
    * @param void empty
    * @return HTTP header
    */
    getBaseURL() {
        let mode = this.applicationMode();
        let baseURL;

        if (mode === 'local') {
            baseURL = 'http://127.0.0.1:8000/api/';
        } else if (mode === 'dev') {
            baseURL = '';
        } else if (mode === 'live') {
            baseURL = '';
        }
        return baseURL;
    }

    getAppUrl() {
        let mode = this.applicationMode();
        let baseURL;

        if (mode === 'local')
            baseURL = '';
        else if (mode === 'dev') {
            baseURL = '';
        } else if (mode === 'live') {
            baseURL = '';
        }
        return baseURL;
    }

    /*
    * get backend Image URL
    * @param void empty
    * @return HTTP header
    * http://127.0.0.1:8000/
    */
    getImageURL() {
        let mode = this.applicationMode();
        let baseURL;

        if (mode === 'local')
            baseURL = 'http://127.0.0.1:8000';
        else if (mode === 'test1') {
            baseURL = '';
        } else if (mode === 'test2') {
            baseURL = '';
        }
        return baseURL;
    }


    getZoomURL() {
        let mode = this.applicationMode();
        let baseURL;
        if (mode === 'dev') {
            baseURL = 'zoom1';
        } else {
            baseURL = 'zoom2';
        }
        return baseURL;
    }

    /*
    * get application mode
    * @param void
    * @return application mode - local, dev , live
    */
    applicationMode(): any {
        let type = 'dev';
        return type;
    }
}
