import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ContainerService {

    public activeRoute = new BehaviorSubject('activeRoute');

    constructor() { }

    setActiveRoute(route) {
        this.activeRoute.next(route);
    }

    /*
    * get behaviour subject based on the currentRoute
    * @param void
    * @return userRole
    */
    public get currentRouteValue(): any {
        return this.activeRoute.value;
    }


    /*
    * get classDetailAdmin from localStorage
    * @param void 
    * @return loggedInUserId
    */
    getClassDetailAdmin() {
        let detail = localStorage.getItem('classDA');
        detail = JSON.parse(detail)
        return detail;
    }

    /*
    * set classDetailAdmin in localStorage
    * @param classDetail
    * @return void
    */
    setClassDetailAdmin(classDetail: string) {
        let detail = JSON.stringify(classDetail)
        localStorage.setItem('classDA', detail);
    }

    /*
    * Remove classDetailAdmin in localStorage
    * @param void
    * @return void
    */
    removeClassDetailAdmin() {
        localStorage.removeItem('classDA');
    }



}
