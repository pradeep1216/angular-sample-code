import { Injectable } from '@angular/core';
import { AsyncResponse } from '../../../interfaces/async-response';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../../services/common/auth/auth.service';

@Injectable({
    providedIn: 'root'
})
export class ClassroomService {
    constructor(
        private http: HttpClient,
        private auth: AuthService
    ) { }

    /*
      * get all classes
      * @param void
      * @return Promise<AsyncResponse>
      */
    getClasses(): Observable<AsyncResponse> {
        return new Observable(subscribe => {
            this.http.get(this.auth.getBaseURL() + 'class/', this.auth.getHTTPHeader('token'))
                .subscribe(
                    res => {
                        subscribe.next({
                            status: 'success',
                            message: 'Classes found',
                            data: res['data'],
                            count: res['data2']
                        });
                    },
                    error => {
                        subscribe.error({
                            status: 'failed',
                            message: error.error.error
                        });
                    }
                );
        });
    }

	/*
   	* get a class
   	* @param classId
   	* @return Promise<AsyncResponse>
   	*/
    getClass(classId): Observable<AsyncResponse> {
        return new Observable(subscribe => {
            this.http.get(this.auth.getBaseURL() + 'class/' + classId + '/', this.auth.getHTTPHeader('token'))
                .subscribe(
                    res => {
                        subscribe.next({
                            status: 'success',
                            message: 'Class found',
                            data: res['data'][0],
                            class: res['data2']
                        });
                    },
                    error => {
                        subscribe.error({
                            status: 'failed',
                            message: error.error.error
                        });
                    }
                );
        });
    }


	/*
	* Create Class
	* @param object of class data
	* @return Promise<AsyncResponse>
	*/
    createClass(data): Observable<AsyncResponse> {
        return new Observable(subscribe => {
            if (data) {
                this.http.post<any>(this.auth.getBaseURL() + 'class/', data, this.auth.getHTTPHeader('token'))
                    .subscribe(
                        res => {
                            subscribe.next({
                                status: 'success',
                                message: 'Comment created successfully.',
                            });
                        },
                        error => {
                            subscribe.error({
                                status: 'failed',
                                message: error.error.error
                            });
                        }
                    );

            } else {
                subscribe.error({
                    status: 'failed',
                    message: 'Invalid Entry.'
                });
            }
        });
    }


	/*
    * Update a Class
    * @param object of ClassId and class data
    * @return Promise<AsyncResponse>
    */
    updateClass(classId, data): Observable<AsyncResponse> {
        return new Observable(subscribe => {
            if (data) {
                this.http.patch<any>(this.auth.getBaseURL() + 'class/' + classId + '/', data, this.auth.getHTTPHeader('token'))
                    .subscribe(
                        res => {
                            subscribe.next({
                                status: 'success',
                                message: 'Successfully updated.',
                            });
                        },
                        error => {
                            subscribe.error({
                                status: 'failed',
                                message: error.error.error
                            });
                        }
                    );

            } else {
                subscribe.error({
                    status: 'failed',
                    message: 'Invalid Entry.'
                });
            }
        });
    }

	/*
	* Delete a class
	* @param object of classId
	* @return Promise<AsyncResponse>
	*/
    deleteClass(classId): Observable<AsyncResponse> {
        return new Observable(subscribe => {
            this.http.delete<any>(this.auth.getBaseURL() + 'class/' + classId, this.auth.getHTTPHeader('token'))
                .subscribe(
                    res => {
                        subscribe.next({
                            status: 'success',
                            message: 'Class deleted successfully.',
                        });
                    },
                    error => {
                        subscribe.error({
                            status: 'failed',
                            message: error.error.error
                        });
                    }
                );
        });
    }
}
