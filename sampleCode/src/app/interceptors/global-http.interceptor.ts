import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthService } from '../services/common/auth/auth.service';

@Injectable()
export class GlobalHTTPInterceptor implements HttpInterceptor {

    constructor(
        public router: Router,
        private auth: AuthService
    ) {
    }

    // intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    //     return <any>next.handle(req).pipe(
    //         catchError((error) => {
    //             console.log('error is intercept')
    //             console.error(error);
    //             return throwError(error.message);
    //         })
    //     )
    // }

    redirectURL;
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let userType = this.auth.getUserDetails()
        if (userType === 'SA' || userType === 'A' || userType === 'SF') {
            this.redirectURL = '/admin/login'
        } else if (userType === 'ST' || userType === 'P') {
            this.redirectURL = '/user/login'
        } else {
            this.redirectURL = '/admin/login'
        }
        const token: string = 'invald token';

        return <any>next.handle(req).pipe(
            catchError((error) => {
                let handled: boolean = false;
                if (error instanceof HttpErrorResponse) {
                    if (error.error instanceof ErrorEvent) {
                        console.error("Error Event");
                    } else {
                        // console.log(`error status : ${error.status} ${error.statusText}`);
                        switch (error.status) {
                            case 401:      //login
                                this.router.navigateByUrl(this.redirectURL);
                                handled = true;
                                break;
                            case 403:     //forbidden
                                this.router.navigateByUrl(this.redirectURL);
                                handled = true;
                                break;
                        }
                    }
                }
                else {
                    console.error("Other Errors");
                }

                if (handled) {
                    return of(error);
                } else {
                    return throwError(error);
                }

            })
        )
    }
}
