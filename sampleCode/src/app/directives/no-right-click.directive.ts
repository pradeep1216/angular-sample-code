import { Directive } from '@angular/core';

@Directive({
    selector: '[appNoRightClick]'
})
export class NoRightClickDirective {

    constructor() { }

}
