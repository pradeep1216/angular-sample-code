import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LoginComponent } from '../components/common/login/login.component';
import { ErrorComponent } from '../components/errors/error/error.component';
import { ContainerComponent } from '../components/admin/layout/container/container.component';
import { DashboardComponent } from '../components/admin/modules/dashboard/dashboard/dashboard.component';
import { RouterComponent } from '../components/common/router/router.component';
import { SettingsComponent } from '../components/admin/modules/settings/settings.component';
import { UserContainerComponent } from '../components/user/layout/user-container/user-container.component';
import { UserSettingsComponent } from '../components/user/modules/common/user-settings/user-settings.component';
import { UserDashboardViewComponent } from '../components/user/modules/dashBoard/user-dashboard-view/user-dashboard-view.component';
import { VideoViewerComponent } from '../components/user/modules/videos/video-viewer/video-viewer.component';
import { Roles } from '../classes/roles';
import { AuthGuard } from '../services/common/auth/auth.guard';
import { TestComponent } from '../components/test/test.component';
import { LiveClassViewerComponent } from '../components/user/modules/live-class-viewer/live-class-viewer.component';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        RouterModule.forRoot([
            {
                path: '',
                redirectTo: '/user/login',
                pathMatch: 'full'
            }, {
                path: 'admin',
                children: [
                    {
                        path: 'login',
                        component: LoginComponent
                    }, {
                        path: '',
                        component: ContainerComponent,
                        children: [
                            {
                                path: 'home',
                                component: DashboardComponent,
                                canActivate: [AuthGuard],
                                data: { roles: [Roles.SuperAdmin, Roles.Admin, Roles.Staff] },
                            }, {
                                path: 'classrooms',
                                component: RouterComponent,
                                canActivate: [AuthGuard],
                                data: { roles: [Roles.SuperAdmin, Roles.Admin] },
                                loadChildren: () => import('../components/admin/modules/classrooms/classrooms.module').then(m => m.ClassroomsModule)

                            }, {
                                path: 'st-classes',
                                component: RouterComponent,
                                canActivate: [AuthGuard],
                                data: { roles: [Roles.SuperAdmin, Roles.Admin, Roles.Staff] },
                                loadChildren: () => import('../components/admin/modules/staffClasses/staff-classes.module').then(m => m.StaffClassesModule)

                            }, {
                                path: 'timetable',
                                component: RouterComponent,
                                canActivate: [AuthGuard],
                                data: { roles: [Roles.SuperAdmin, Roles.Admin, Roles.Staff] },
                                loadChildren: () => import('../components/admin/modules/timeTable/time-table.module').then(m => m.TimeTableModule)
                            }, {
                                path: 'users',
                                component: RouterComponent,
                                canActivate: [AuthGuard],
                                data: { roles: [Roles.SuperAdmin, Roles.Admin] },
                                loadChildren: () => import('../components/admin/modules/users/users.module').then(m => m.UsersModule)
                            }, {
                                path: 'settings',
                                canActivate: [AuthGuard],
                                data: { roles: [Roles.SuperAdmin, Roles.Admin, Roles.Staff] },
                                component: SettingsComponent
                            }, {
                                path: 'staff-assignments',
                                component: RouterComponent,
                                canActivate: [AuthGuard],
                                data: { roles: [Roles.Staff] },
                                loadChildren: () => import('../components/admin/modules/staff-assignments/staff-assignments.module').then(m => m.StaffAssignmentsModule)
                            }, {
                                path: 'staff-attendance',
                                component: RouterComponent,
                                canActivate: [AuthGuard],
                                data: { roles: [Roles.Staff] },
                                loadChildren: () => import('../components/admin/modules/staff-attendance/staff-attendance.module').then(m => m.StaffAttendanceModule)
                            }, {
                                path: 'staff-qa',
                                component: RouterComponent,
                                canActivate: [AuthGuard],
                                data: { roles: [Roles.Staff] },
                                loadChildren: () => import('../components/admin/modules/staff-discussions/staff-discussions.module').then(m => m.StaffDiscussionsModule)
                            }, {
                                path: 'staff-tests',
                                component: RouterComponent,
                                canActivate: [AuthGuard],
                                data: { roles: [Roles.Staff] },
                                loadChildren: () => import('../components/admin/modules/staff-unit-tests/staff-unit-tests.module').then(m => m.StaffUnitTestsModule)
                            }, {
                                path: 'timetable',
                                component: RouterComponent,
                                canActivate: [AuthGuard],
                                data: { roles: [Roles.SuperAdmin, Roles.Admin] },
                                loadChildren: () => import('../components/admin/modules/timeTable/time-table.module').then(m => m.TimeTableModule)
                            }, {
                                path: 'class-manager',
                                component: RouterComponent,
                                canActivate: [AuthGuard],
                                data: { roles: [Roles.SuperAdmin, Roles.Admin] },
                                loadChildren: () => import('../components/admin/modules/admin-class-manager/admin-class-manager.module').then(m => m.AdminClassManagerModule)
                            }
                        ]
                    }
                ]
            }, {
                path: 'user',
                children: [
                    {
                        path: 'login',
                        component: LoginComponent
                    }, {
                        path: '',
                        component: UserContainerComponent,
                        children: [
                            {
                                path: 'home',
                                component: UserDashboardViewComponent
                            }, {
                                path: ':file_id/videos',
                                component: VideoViewerComponent
                            }, {
                                path: ':file_id/live',
                                component: LiveClassViewerComponent
                            }, {
                                path: 'student-qa',
                                component: RouterComponent,
                                loadChildren: () => import('../components/user/modules/student-qa/student-qa.module').then(m => m.StudentQAModule)
                            }, {
                                path: 'parent-qa',
                                component: RouterComponent,
                                loadChildren: () => import('../components/user/modules/parent-qa/parent-qa.module').then(m => m.ParentQAModule)
                            }, {
                                path: 'st-dashboard',
                                component: RouterComponent,
                                loadChildren: () => import('../components/user/modules/parentStDashboard/parent-st-dashboard.module').then(m => m.ParentStDashboardModule)
                            }, {
                                path: 'student-tests',
                                component: RouterComponent,
                                loadChildren: () => import('../components/user/modules/student-unit-tests/student-unit-tests.module').then(m => m.StudentUnitTestsModule)
                            }, {
                                path: 'student-assignments',
                                component: RouterComponent,
                                loadChildren: () => import('../components/user/modules/student-assignments/student-assignments.module').then(m => m.StudentAssignmentsModule)
                            }, {
                                path: 'student-timetable',
                                component: RouterComponent,
                                loadChildren: () => import('../components/user/modules/student-timetable/student-timetable.module').then(m => m.StudentTimetableModule)
                            }, {
                                path: 'student-attendance',
                                component: RouterComponent,
                                loadChildren: () => import('../components/user/modules/student-attendance/student-attendance.module').then(m => m.StudentAttendanceModule)
                            }, {
                                path: 'settings',
                                component: UserSettingsComponent
                            }
                        ]

                    }
                ]
            }, {
                path: 'access-forbidden',
                component: ErrorComponent
            }, {
                path: '**',
                component: ErrorComponent
            }

        ])
    ]
})
export class RoutesModule { }
