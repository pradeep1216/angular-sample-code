import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Convert24to12Pipe } from '../pipes/convert24to12.pipe';
import { FullDayPipe } from '../pipes/full-day.pipe';
import { SafePipe } from '../pipes/safe.pipe';
import { ClassTypePipe } from '../pipes/class-type.pipe';
import { SafeHTMLPipe } from '../pipes/safe-html.pipe';
import { FormatTimePipe } from '../pipes/format-time.pipe';



@NgModule({
    declarations: [
        Convert24to12Pipe,
        FullDayPipe,
        SafePipe,
        ClassTypePipe,
        SafeHTMLPipe,
        FormatTimePipe
    ],
    imports: [
        CommonModule,

    ], exports: [
        Convert24to12Pipe,
        FullDayPipe,
        SafePipe,
        ClassTypePipe,
        SafeHTMLPipe,
        FormatTimePipe
    ]
})
export class PipesModule { }
