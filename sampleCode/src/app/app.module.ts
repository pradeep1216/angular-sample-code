import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/common/login/login.component';
import { ErrorComponent } from './components/errors/error/error.component';
import { RoutesModule } from './modules/routes.module';
import { MaterialModule } from './modules/material.module';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterComponent } from './components/common/router/router.component';
import { ContainerComponent } from './components/admin/layout/container/container.component';
import { LayoutModule } from '@angular/cdk/layout';
import { DashboardComponent } from './components/admin/modules/dashboard/dashboard/dashboard.component';
import { SettingsComponent } from './components/admin/modules/settings/settings.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { UserContainerComponent } from './components/user/layout/user-container/user-container.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { QuillModule } from 'ngx-quill';
import { PipesModule } from './modules/pipes.module';
import { VjsPlayerComponent } from './components/reusable/vjs-player/vjs-player.component';

import { DeviceDetectorModule } from 'ngx-device-detector';
import { GlobalHTTPInterceptor } from './interceptors/global-http.interceptor';

import { VgCoreModule, } from '@videogular/ngx-videogular/core';
import { VgControlsModule } from '@videogular/ngx-videogular/controls';
import { VgOverlayPlayModule } from '@videogular/ngx-videogular/overlay-play';
import { VgBufferingModule } from '@videogular/ngx-videogular/buffering';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        ErrorComponent,
        RouterComponent,
        ContainerComponent,
        DashboardComponent,
        SettingsComponent,
        UserContainerComponent,
        VjsPlayerComponent,
    ],
    imports: [
        BrowserModule,
        VgCoreModule,
        VgControlsModule,
        VgOverlayPlayModule,
        VgBufferingModule,
        BrowserAnimationsModule,
        RoutesModule,
        MaterialModule,
        RouterModule,
        HttpClientModule,
        ReactiveFormsModule,
        LayoutModule,
        BsDropdownModule.forRoot(),
        ModalModule.forRoot(),
        BsDatepickerModule.forRoot(),
        QuillModule.forRoot(
            {
                modules: {
                    toolbar: [
                        ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
                        ['blockquote', 'code-block'],

                        [{ 'header': 1 }, { 'header': 2 }],               // custom button values
                        [{ 'list': 'ordered' }, { 'list': 'bullet' }],
                        [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
                        [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
                        [{ 'direction': 'rtl' }],                         // text direction

                        [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
                        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

                        [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
                        [{ 'font': [] }],
                        [{ 'align': [] }],

                        ['clean'],                                         // remove formatting button
                    ]
                }
            }
        ),
        PipesModule,
        DeviceDetectorModule,
        FormsModule
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: GlobalHTTPInterceptor, multi: true }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
